var hierarchy =
[
    [ "MyF1.Data.csapatok", "class_my_f1_1_1_data_1_1csapatok.html", null ],
    [ "MyF1.Data.csapatXpilota", "class_my_f1_1_1_data_1_1csapat_xpilota.html", null ],
    [ "DbContext", null, [
      [ "MyF1::Data::AdatbazisEntities", "class_my_f1_1_1_data_1_1_adatbazis_entities.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "MyF1.Repository.Repository", "class_my_f1_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "MyF1.Logic.ILogic", "interface_my_f1_1_1_logic_1_1_i_logic.html", [
      [ "MyF1.Logic.Logic", "class_my_f1_1_1_logic_1_1_logic.html", null ]
    ] ],
    [ "MyF1.Repository.IRepository", "interface_my_f1_1_1_repository_1_1_i_repository.html", [
      [ "MyF1.Repository.Repository", "class_my_f1_1_1_repository_1_1_repository.html", null ]
    ] ],
    [ "MyF1.Logic.Tests.LogicTest", "class_my_f1_1_1_logic_1_1_tests_1_1_logic_test.html", null ],
    [ "MyF1.Data.motorGyartok", "class_my_f1_1_1_data_1_1motor_gyartok.html", null ],
    [ "MyF1.Data.pilota", "class_my_f1_1_1_data_1_1pilota.html", null ],
    [ "MyF1.Program.Program", "class_my_f1_1_1_program_1_1_program.html", null ]
];