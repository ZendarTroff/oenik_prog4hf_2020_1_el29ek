var annotated_dup =
[
    [ "MyF1", null, [
      [ "Data", null, [
        [ "AdatbazisEntities", "class_my_f1_1_1_data_1_1_adatbazis_entities.html", null ],
        [ "csapatok", "class_my_f1_1_1_data_1_1csapatok.html", "class_my_f1_1_1_data_1_1csapatok" ],
        [ "csapatXpilota", "class_my_f1_1_1_data_1_1csapat_xpilota.html", "class_my_f1_1_1_data_1_1csapat_xpilota" ],
        [ "motorGyartok", "class_my_f1_1_1_data_1_1motor_gyartok.html", "class_my_f1_1_1_data_1_1motor_gyartok" ],
        [ "pilota", "class_my_f1_1_1_data_1_1pilota.html", "class_my_f1_1_1_data_1_1pilota" ]
      ] ],
      [ "Logic", "namespace_my_f1_1_1_logic.html", "namespace_my_f1_1_1_logic" ],
      [ "Program", "namespace_my_f1_1_1_program.html", "namespace_my_f1_1_1_program" ],
      [ "Repository", "namespace_my_f1_1_1_repository.html", "namespace_my_f1_1_1_repository" ]
    ] ]
];