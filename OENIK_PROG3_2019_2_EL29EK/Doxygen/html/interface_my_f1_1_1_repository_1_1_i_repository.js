var interface_my_f1_1_1_repository_1_1_i_repository =
[
    [ "CsapatokCreate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a9818831d170b185943eeb9d9f12a4764", null ],
    [ "CsapatokDelete", "interface_my_f1_1_1_repository_1_1_i_repository.html#a48e4685fba8c9c6e162e421f15ca921f", null ],
    [ "CsapatokGetAll", "interface_my_f1_1_1_repository_1_1_i_repository.html#ab7b29e614bf63c8c678f4e8a4f8c4a70", null ],
    [ "CsapatokGetOne", "interface_my_f1_1_1_repository_1_1_i_repository.html#aef19fd82036d8130d9f46fa90fc6adbb", null ],
    [ "CsapatokUpdate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a23ba1f534fa479394ebd2b1262aaeeeb", null ],
    [ "CsapatXPilotaCreate", "interface_my_f1_1_1_repository_1_1_i_repository.html#aeeadfb5f10bf6783d947bd36c0d7370f", null ],
    [ "CsapatXPilotaDelete", "interface_my_f1_1_1_repository_1_1_i_repository.html#a5bc3436daa1a96a17cd0fca457a05cf4", null ],
    [ "CsapatXpilotaGetAll", "interface_my_f1_1_1_repository_1_1_i_repository.html#af13e06fb54c1c11cf88fff2669f0eb84", null ],
    [ "CsapatXpilotaGetOne", "interface_my_f1_1_1_repository_1_1_i_repository.html#a05c6a6bb89817b37bde580f5f15f0732", null ],
    [ "CsapatXPilotaUpdate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a6035627b6a8e6e3b54d71d478e9704a1", null ],
    [ "MotorGyartokCreate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a0e4baa043741f2d3d4caf92b78eaa104", null ],
    [ "MotorGyartokDelete", "interface_my_f1_1_1_repository_1_1_i_repository.html#a4dae4a87609d993472931f8b21e4a375", null ],
    [ "MotorgyartokGetAll", "interface_my_f1_1_1_repository_1_1_i_repository.html#a3eda1184747d978d1d5c5f1ac429da34", null ],
    [ "MotorGyartokGetOne", "interface_my_f1_1_1_repository_1_1_i_repository.html#abeebb73f401781f391e3b916fc3f5d8d", null ],
    [ "MotorGyartokUpdate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a428a26bb269b9bf080d2323415cc16a7", null ],
    [ "PilotaCreate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a4b5bf8487f8ab65500f634bcd26b7fcc", null ],
    [ "PilotaDelete", "interface_my_f1_1_1_repository_1_1_i_repository.html#af2b82ea02e68dffeac3f0dd30dfb1c2a", null ],
    [ "PilotaGetAll", "interface_my_f1_1_1_repository_1_1_i_repository.html#a0aa191cf12665476a41c8baae00bc823", null ],
    [ "PilotaGetOne", "interface_my_f1_1_1_repository_1_1_i_repository.html#ab3e590cff31e5b7ec25ec9d0ef235b30", null ],
    [ "PilotaUpdate", "interface_my_f1_1_1_repository_1_1_i_repository.html#a63a75371603403e13598f6cc02ca5b2b", null ]
];