﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Threading;

namespace OtherHW.Pong
{
    public static class Config // Alternative: see app.xaml
    {
        public static double Width = 700;
        public static double Height = 300;
        public static int BorderSize = 4;
        public static Brush BorderColor = Brushes.DarkGray;
        public static Brush BgColor = Brushes.Cyan;

        public static Brush BallBg = Brushes.Yellow;
        public static Brush BallLine = Brushes.Red;
        public static Brush PadBg = Brushes.Maroon;
        public static Brush PadLine = Brushes.Black;

        public static int BallSize = 20;
        public static int PadWidth = 100;
        public static int PadHeight = 20;
        public static int EnemyHT = 20;
        public static Brush EnemyBg = Brushes.Blue;
        public static Brush EnemyBL = Brushes.Blue;
    }

    class MyShape
    {
        Rect area;
        public Rect Area
        {
            get { return area; } // NO! get;
        }

        public int Dx { get; set; }
        public int Dy { get; set; }

        public MyShape(double x, double y, double w, double h)
        {
            area = new Rect(x, y, w, h);
            Dx = 5;
            Dy = 5;
        }
        public void ChangeX(double diff)
        {
            // Area.X += diff; // Not a variable!
            // Area = new Rect(Area.X+diff, xxxx) // Slow!
            area.X += diff;
        }
        public void ChangeY(double diff)
        {
            area.Y += diff;
        }
        public void SetXY(double x, double y)
        {
            area.X = x;
            area.Y = y;
        }
    }

    class PongModel
    {
        public int Errors { get; set; }
        public MyShape Pad { get; set; }
        public MyShape Ball { get; set; }

        public List<Enemy> Enemy { get; set; }
        //public List<Enemy> Stars { get; set; }  // Phase 2 - No time?
        //Random rnd =new Random();
        public PongModel()
        {
            Pad = new MyShape(Config.Width / 2, Config.Height - 20, 100, 20);
            Ball = new MyShape(Config.Width / 2, Config.Height / 2, 20, 20);
            Enemy = new List<Enemy>();


            //Stars = new List<Enemy>(); // Phase 2
        }
    }

    class PongLogic
    {
        PongModel model;
        public enum Direction { Left, Right }
        public event EventHandler RefreshScreen; // instead of NotifyPropertyChanged

        public PongLogic(PongModel model)
        {
            this.model = model;
            while (model.Enemy.Count < 3)
            {
                AddEnemy();
            }
        }

        public void MovePad(Direction d)
        {
            if (d == Direction.Left)
            {
                model.Pad.ChangeX(-10);
            }
            else
            {
                model.Pad.ChangeX(10);
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public void JumpPad(double x)
        {
            model.Pad.SetXY(x, model.Pad.Area.Y);
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        public bool MoveShape(MyShape shape)
        {
            bool faulted = false;
            shape.ChangeX(shape.Dx);
            shape.ChangeY(shape.Dy);

            if (shape.Area.Left < 0 || shape.Area.Right > Config.Width)
            {
                shape.Dx = -shape.Dx;
            }

            if (shape.Area.Top < 0 || shape.Area.IntersectsWith(model.Pad.Area))
            {
                shape.Dy = -shape.Dy;
            }
            if (shape.Area.Bottom > Config.Height)
            {
                shape.SetXY(shape.Area.X, Config.Height / 2);
                faulted = true;
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);
            return faulted;
        }

        public void MoveBall()
        {
            if (MoveShape(model.Ball)) model.Errors++;
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public void MoveEnemy()
        {
            foreach (Enemy item in model.Enemy)
            {
                if (EnemyColisonMove(item))
                {
                    Colision(item);
                }
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }

        private void Colision(Enemy item)
        {
            item.SetXY(rnd.Next(1, 699), 0);
            int ballDir = rnd.Next(1, 4);
            if (ballDir == 1)
            {
                model.Ball.Dx = -model.Ball.Dx;
                model.Ball.Dy = -model.Ball.Dy;
            }
            if (ballDir == 2)
            {
                model.Ball.Dx = model.Ball.Dx;
                model.Ball.Dy = -model.Ball.Dy;
            }
            if (ballDir == 3)
            {
                model.Ball.Dx = -model.Ball.Dx;
                model.Ball.Dy = -model.Ball.Dy;
            }
            if (ballDir == 4)
            {
                model.Ball.Dx = -model.Ball.Dx;
                model.Ball.Dy = model.Ball.Dy;
            }
            RefreshScreen?.Invoke(this, EventArgs.Empty);

        }

        static Random rnd = new Random();

        public void AddEnemy() // Phase 2
        {
            model.Enemy.Add(new Enemy(rnd.Next(0, 699), 0));

            RefreshScreen?.Invoke(this, EventArgs.Empty);
        }
        public bool EnemyColisonMove(MyShape shape) // Phase 2
        {
            bool colision = false;

            shape.ChangeX(rnd.Next(-5, 6));
            shape.ChangeY(rnd.Next(-5, 6));

            if (shape.Area.Left < 0)
            {
                shape.ChangeX(1);
            }
            if (shape.Area.Right > Config.Width)
            {
                shape.ChangeX(-1);
            }
            if (shape.Area.Top < 1)
            {
                shape.ChangeY(1);
            }
            if (shape.Area.Bottom > Config.Height - 21)
            {
                shape.ChangeY(-1);
            }
            if (shape.Area.IntersectsWith(model.Ball.Area))
            {
                colision = true;
            }

            RefreshScreen?.Invoke(this, EventArgs.Empty);
            return colision;
        }
    }

    class PongRenderer
    {
        PongModel model;

        public PongRenderer(PongModel model)
        {
            this.model = model;
        }

        public void DrawThings(DrawingContext ctx)
        {
            DrawingGroup dg = new DrawingGroup();

            GeometryDrawing background = new GeometryDrawing(Config.BgColor,
                new Pen(Config.BorderColor, Config.BorderSize),
                new RectangleGeometry(new Rect(0, 0, Config.Width, Config.Height)));
            GeometryDrawing ball = new GeometryDrawing(Config.BallBg,
                new Pen(Config.BallLine, 1),
                new EllipseGeometry(model.Ball.Area));
            GeometryDrawing pad = new GeometryDrawing(Config.PadBg,
                new Pen(Config.PadLine, 1),
                new RectangleGeometry(model.Pad.Area));
            FormattedText formattedText = new FormattedText(model.Errors.ToString(),
                System.Globalization.CultureInfo.CurrentCulture,
                FlowDirection.LeftToRight,
                new Typeface("Arial"),
                16,
                Brushes.Black);
            GeometryDrawing text = new GeometryDrawing(null, new Pen(Brushes.Red, 2),
                formattedText.BuildGeometry(new Point(5, 5)));

            dg.Children.Add(background);
            dg.Children.Add(ball);
            dg.Children.Add(pad);
            dg.Children.Add(text);

            foreach (Enemy item in model.Enemy)
            {
                GeometryDrawing enemy = new GeometryDrawing(Config.EnemyBg,
               new Pen(Config.EnemyBL, 1),
               new RectangleGeometry(item.Area));
                dg.Children.Add(enemy);
            }

            ctx.DrawDrawing(dg);
        }
    }

    class PongControl : FrameworkElement
    {
        PongModel model;
        PongLogic logic;
        PongRenderer renderer;
        DispatcherTimer tickTimer;

        public PongControl()
        {
            Loaded += GameScreen_Loaded; // += <TAB><RET>
            // PongControl ctrl = new PongControl();
            // someWindow.Content = ctrl; ... XAML
        }

        private void GameScreen_Loaded(object sender, RoutedEventArgs e)
        {
            model = new PongModel();
            logic = new PongLogic(model);
            renderer = new PongRenderer(model);

            Window win = Window.GetWindow(this);
            if (win != null) // if (!IsInDesignMode)
            {

                tickTimer = new DispatcherTimer();
                tickTimer.Interval = TimeSpan.FromMilliseconds(25);
                tickTimer.Tick += timer_Tick;
                tickTimer.Start();

                win.KeyDown += Win_KeyDown; // += <TAB><RET>
                MouseLeftButtonDown += PongControl_MouseLeftButtonDown; // += <TAB><RET>
            }

            logic.RefreshScreen += (obj, args) => InvalidateVisual();
            InvalidateVisual();
        }

        private void PongControl_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            logic.JumpPad(e.GetPosition(this).X);
        }

        private void Win_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Left: logic.MovePad(PongLogic.Direction.Left); break;
                case Key.Right: logic.MovePad(PongLogic.Direction.Right); break;
                case Key.Space: logic.AddEnemy(); break; // phase 2
            }
        }

        void timer_Tick(object sender, EventArgs e)
        {
            logic.MoveBall();
            logic.MoveEnemy(); // phase 2
        }

        protected override void OnRender(DrawingContext drawingContext)
        {
            if (renderer != null) renderer.DrawThings(drawingContext);
        }
    }

    class Enemy : MyShape // Phase 2
    {
        public Enemy(double x, double y) : base(x, y, Config.EnemyHT, Config.EnemyHT)
        {
        }

    }
}
