﻿
CREATE TABLE motorGyartok (
	ID numeric(5) not null,
    gyartoNev VARCHAR(200),
    alapitasEve NUMERIC(4) NOT NULL,
    nemzetiseg VARCHAR(200),
    kozpont VARCHAR(200),
    loEro NUMERIC(4) NOT NULL,
    gyozelmekSzama NUMERIC(4) NOT NULL,
    CONSTRAINT MOTORGYARTOK_PRIMARY_KEY PRIMARY KEY (ID));
    


CREATE TABLE csapatok (
	ID numeric(5) not null,
	csapatNev VARCHAR(200),
	alapitasEve NUMERIC(4) NOT NULL,
	kozpont VARCHAR(200),
	csapatSzin VARCHAR(200),
	gyozelmekSzama NUMERIC(4) NOT NULL,
	bajnokiCimekSzama NUMERIC(3) NOT NULL,
	gyartoID numeric(5) not null,
	CONSTRAINT CSAPATOK_PRIMARY_KEY PRIMARY KEY (ID),
        CONSTRAINT CSAPATOK_FOREIGN_KEY FOREIGN KEY (gyartoID)REFERENCES motorGyartok(ID));

CREATE TABLE pilota (
	pilotaID NUMERIC(2) NOT NULL,
	nev VARCHAR(200),
	karrierKezdesEve NUMERIC(4) NOT NULL,
	nemzetiseg VARCHAR(200),
	gyozelmekSzama NUMERIC(4) NOT NULL,
	bajnokiCimekSzama NUMERIC(2) NOT NULL,
	CONSTRAINT PILOTA_PRIMARY_KEY PRIMARY KEY (pilotaID));

CREATE TABLE csapatXpilota (
	szerzodesID NUMERIC(3) NOT NULL,
	csapatID numeric(5) not null,
	pilotaID NUMERIC(2) NOT NULL,
	CONSTRAINT CSAPATXPILOTA_PRIMARY_KEY PRIMARY KEY (szerzodesID),
	CONSTRAINT CSAPATXPILOTA_FOREIGN_KEY FOREIGN KEY (csapatID)REFERENCES csapatok(ID),
	CONSTRAINT CSAPATXPILOTA2_FOREIGN_KEY FOREIGN KEY (pilotaID)REFERENCES pilota(pilotaID));


INSERT INTO motorGyartok VALUES (1,'Vanwall',1949,'UK','Maidenhead',290,10);
INSERT INTO motorGyartok VALUES (2,'Climax',1903,'UK','Coventry',220,8);
INSERT INTO motorGyartok VALUES (3,'Ferrari',1929,'ITA','Maranello',1010,300);
INSERT INTO motorGyartok VALUES (4,'BRM',1947,'UK','Bourne',230,17);
INSERT INTO motorGyartok VALUES (5,'Repco',1922,'AUS','Melbourne',255,8);
INSERT INTO motorGyartok VALUES (6,'Ford',1903,'USA','Michigan',330,58);
INSERT INTO motorGyartok VALUES (7,'Honda',1948,'JAP','Tokio',360,57);
INSERT INTO motorGyartok VALUES (8,'TAG',1931,'D','Stuttgart',355,18);
INSERT INTO motorGyartok VALUES (9,'Renault',1898,'FR','Boulogne',370,115);
INSERT INTO motorGyartok VALUES (10,'Mercedes',1926,'D','Stuttgart',900,110);


INSERT INTO csapatok VALUES (1,'BRM',1947,'Bourne','Dark Black',17,1,4);
INSERT INTO csapatok VALUES (2,'Vanwall',1949,'Maidenhead','Light Green',10,1,1);
INSERT INTO csapatok VALUES (3,'Ferrari',1929,'Maranello','Red',300,16,3);
INSERT INTO csapatok VALUES (4,'Cooper',1946,'Surbiton','Dark Blue',16,2,2);
INSERT INTO csapatok VALUES (5,'Brabham',1961,'Milton Keynes','Dark Green',35,2,5);
INSERT INTO csapatok VALUES (6,'Lotus',1948,'Hethel','Green',74,7,6);
INSERT INTO csapatok VALUES (7,'Matra',1940,'Paris','Light Blue',9,1,6);
INSERT INTO csapatok VALUES (8,'Tyrrell',1958,'Ockham','Deep Dark Blue',33,1,6);
INSERT INTO csapatok VALUES (9,'McLaren',1966,'Woking','Black White',182,8,7);
INSERT INTO csapatok VALUES (10,'Williams',1977,'Grove','White',114,9,7);
INSERT INTO csapatok VALUES (11,'Benetton',1983,'Enstone','Sea Blue',27,2,9);
INSERT INTO csapatok VALUES (12,'Renault',1898,'Enstone','Yellow',35,2,9);
INSERT INTO csapatok VALUES (13,'Brawn',2009,'Brackley','White Yellow',8,1,10);
INSERT INTO csapatok VALUES (14,'Red Bull',2005,'Milton Keynes','Purple',61,4,9);
INSERT INTO csapatok VALUES (15,'Mercedes',2010,'Brackley','Silver',98,6,10);

INSERT INTO pilota VALUES (1,'Valtteri Bottas',2011,'Finn',6,0);
INSERT INTO pilota VALUES (2,'Lewis Hamilton',2007,'Brit',82,5);
INSERT INTO pilota VALUES (3,'Nico Rosberg',2006,'Nemet',23,1);
INSERT INTO pilota VALUES (4,'Mark Webber',2002,'Ausztral',9,0);
INSERT INTO pilota VALUES (5,'Sebastian Vettel',2007,'Nemet',53,4);
INSERT INTO pilota VALUES (6,'Rubens Barrichello',1993,'Brazil',11,0);
INSERT INTO pilota VALUES (7,'Jenson Button',2000,'Brit',15,1);
INSERT INTO pilota VALUES (8,'Felipe Massa',2002,'Brazil',11,0);
INSERT INTO pilota VALUES (9,'Giancarlo Fisichella',1996,'Olasz',3,0);
INSERT INTO pilota VALUES (10,'Kimi Räikkönen',2001,'Finn',21,1);
INSERT INTO pilota VALUES (11,'Fernando Alonso',2001,'Spanyol',32,2);
INSERT INTO pilota VALUES (12,'Michael Schumacher',1991,'Nemet',91,7);
INSERT INTO pilota VALUES (13,'Mika Hakkinen',1991,'Brit',20,2);
INSERT INTO pilota VALUES (14,'David Coulthard',1994,'Finn',13,0);
INSERT INTO pilota VALUES (15,'Jacques Villeneuve',1996,'Kanada',11,1);
INSERT INTO pilota VALUES (16,'Valtteri Bottas',2011,'Finn',6,0);
INSERT INTO pilota VALUES (17,'Damon Hill',1992,'Brit',22,1);
INSERT INTO pilota VALUES (18,'Alain Prost',1980,'Francia',51,4);
INSERT INTO pilota VALUES (19,'Nigel Mansell',1980,'Brit',31,1);
INSERT INTO pilota VALUES (20,'Ayrton Senna',1984,'Brazil',41,3);
INSERT INTO pilota VALUES (21,'Nelson Piquet',1978,'Brazil',23,3);
INSERT INTO pilota VALUES (22,'Niki Lauda',1971,'Osztrak',25,3);
INSERT INTO pilota VALUES (23,'Alan Jones',1975,'Ausztral',12,1);
INSERT INTO pilota VALUES (24,'Mario Andretti',1968,'USA',12,1);
INSERT INTO pilota VALUES (25,'Jody Scheckter',1972,'Del Afrika',10,1);
INSERT INTO pilota VALUES (26,'Emerson Fittipaldi',1970,'Brazil',14,2);
INSERT INTO pilota VALUES (27,'Jackie Stewart',1965,'Brit',27,3);
INSERT INTO pilota VALUES (28,'Jochen Rindt',1964,'Osztrak',6,1);
INSERT INTO pilota VALUES (29,'Graham Hill',1958,'Brit',14,2);
INSERT INTO pilota VALUES (30,'Denny Hulme',1965,'Uj Zeland',8,1);
INSERT INTO pilota VALUES (31,'Jim Clark',1960,'Brit',25,2);
INSERT INTO pilota VALUES (32,'John Surtees',1960,'Brit',6,1);
INSERT INTO pilota VALUES (33,'Phil Hill',1958,'USA',3,1);
INSERT INTO pilota VALUES (34,'Jack Brabham',1955,'Ausztral',14,3);
INSERT INTO pilota VALUES (35,'Stirling Moss',1951,'Brit',16,1);

INSERT INTO csapatxpilota VALUES (1,1,29);
INSERT INTO csapatxpilota VALUES (2,2,35);
INSERT INTO csapatxpilota VALUES (3,3,10);
INSERT INTO csapatxpilota VALUES (4,3,12);
INSERT INTO csapatxpilota VALUES (5,3,25);
INSERT INTO csapatxpilota VALUES (6,3,22);
INSERT INTO csapatxpilota VALUES (7,3,32);
INSERT INTO csapatxpilota VALUES (8,3,33);
INSERT INTO csapatxpilota VALUES (9,4,34);
INSERT INTO csapatxpilota VALUES (10,5,34);
INSERT INTO csapatxpilota VALUES (11,5,30);
INSERT INTO csapatxpilota VALUES (12,5,21);
INSERT INTO csapatxpilota VALUES (13,6,31);
INSERT INTO csapatxpilota VALUES (14,6,29);
INSERT INTO csapatxpilota VALUES (15,6,26);
INSERT INTO csapatxpilota VALUES (16,6,24);
INSERT INTO csapatxpilota VALUES (17,6,28);
INSERT INTO csapatxpilota VALUES (18,7,27);
INSERT INTO csapatxpilota VALUES (19,8,27);
INSERT INTO csapatxpilota VALUES (20,9,26);
INSERT INTO csapatxpilota VALUES (21,9,22);
INSERT INTO csapatxpilota VALUES (22,9,18);
INSERT INTO csapatxpilota VALUES (23,9,20);
INSERT INTO csapatxpilota VALUES (24,9,22);
INSERT INTO csapatxpilota VALUES (25,9,13);
INSERT INTO csapatxpilota VALUES (26,9,2);
INSERT INTO csapatxpilota VALUES (27,10,23);
INSERT INTO csapatxpilota VALUES (28,10,21);
INSERT INTO csapatxpilota VALUES (29,10,19);
INSERT INTO csapatxpilota VALUES (30,10,18);
INSERT INTO csapatxpilota VALUES (31,10,17);
INSERT INTO csapatxpilota VALUES (32,10,15);
INSERT INTO csapatxpilota VALUES (33,11,12);
INSERT INTO csapatxpilota VALUES (34,12,11);
INSERT INTO csapatxpilota VALUES (35,13,7);
INSERT INTO csapatxpilota VALUES (36,14,5);
INSERT INTO csapatxpilota VALUES (37,15,1);
INSERT INTO csapatxpilota VALUES (38,15,2);
INSERT INTO csapatxpilota VALUES (39,15,3);