﻿using GalaSoft.MvvmLight.Messaging;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace MyF1.Wpf
{
    class MainLogic
    {
        string url = "http://localhost:7223/api/MotorGyartokApi/";
        HttpClient client = new HttpClient();

        void SendMessage(bool siker)
        {
            string msg = siker ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "MotorGyartokResult");
        }
        public List<MotorGyartoVM> ApiGetMotoGy()
        {
            string json = client.GetStringAsync(url + "all").Result;
            var list = JsonConvert.DeserializeObject<List<MotorGyartoVM>>(json);
            //nem szabad :(
            //SendMessage(true);
            return list;
        }
        public void ApiDelMotoGy(MotorGyartoVM mgy)
        {
            bool success = false;
            if (mgy!=null)
            {
                string json = client.GetStringAsync(url + "del/" + mgy.Id.ToString()).Result;
                JObject obj = JObject.Parse(json);
                success = (bool)obj["OperationResult"];
            }
            SendMessage(success);
        }
        bool ApiEditMotoGy(MotorGyartoVM mgy,bool isEditing)
        {
            if (mgy == null) return false;
            string myUrl = isEditing ? url + "mod" : url + "add";

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing) postData.Add(nameof(MotorGyartoVM.Id), mgy.Id.ToString());
            postData.Add(nameof(MotorGyartoVM.Nev), mgy.Nev);
            postData.Add(nameof(MotorGyartoVM.AlapitasEve), mgy.AlapitasEve.ToString());
            postData.Add(nameof(MotorGyartoVM.Nemzetiseg), mgy.Nemzetiseg);
            postData.Add(nameof(MotorGyartoVM.Kozpont), mgy.Kozpont);
            postData.Add(nameof(MotorGyartoVM.LoEro), mgy.LoEro.ToString());
            postData.Add(nameof(MotorGyartoVM.GyozelmekSzama), mgy.GyozelmekSzama.ToString());

            string json = client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JObject obj = JObject.Parse(json);
            return (bool)obj["OperationResult"];
        }
        public void EditMotoGy(MotorGyartoVM mgy,Func<MotorGyartoVM,bool>editor)
        {
            MotorGyartoVM clone = new MotorGyartoVM();
            if (mgy != null) clone.CopyFrom(mgy);
            bool? success = editor?.Invoke(clone);
            if (success == true)
            {
                if (mgy != null)success= ApiEditMotoGy(clone, true);
                else success= ApiEditMotoGy(clone, false);
            }
            SendMessage(success==true);
        }
    }
}
