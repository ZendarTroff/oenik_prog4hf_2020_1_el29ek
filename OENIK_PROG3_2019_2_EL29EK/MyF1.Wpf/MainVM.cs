﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.CommandWpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace MyF1.Wpf
{
    class MainVM:ViewModelBase
    {
        private MainLogic logic;
        private MotorGyartoVM selectedMotoGy;
        private ObservableCollection<MotorGyartoVM> allMotoGy;

        public MotorGyartoVM SelectedMotoGy
        {
            get { return selectedMotoGy; }
            set { Set(ref selectedMotoGy, value); }
        }

        public ObservableCollection<MotorGyartoVM> AllMotoGy
        { get { return allMotoGy; }
            set { Set(ref allMotoGy, value); }
        }
        public ICommand AddCmd { get; private set; }
        public ICommand DelCmd { get; private set; }
        public ICommand ModCmd { get; private set; }
        public ICommand LoadCmd { get; private set; }

        public Func<MotorGyartoVM, bool> EditorFunc { get; set; }
        public MainVM()
        {
            logic = new MainLogic();

            DelCmd = new RelayCommand(() => logic.ApiDelMotoGy(selectedMotoGy));
            AddCmd = new RelayCommand(() => logic.EditMotoGy(null, EditorFunc));
            ModCmd = new RelayCommand(() => logic.EditMotoGy(selectedMotoGy, EditorFunc));
            LoadCmd = new RelayCommand(()=>  AllMotoGy=new ObservableCollection<MotorGyartoVM>(logic.ApiGetMotoGy()));

        }
    }
}
