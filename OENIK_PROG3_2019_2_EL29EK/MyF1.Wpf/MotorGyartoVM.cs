﻿using GalaSoft.MvvmLight;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyF1.Wpf
{
    class MotorGyartoVM:ObservableObject
    {
        private int id;
        private string nev;
        private int alapitasEve;
        private string nemzetiseg;
        private string kozpont;
        private int loEro;
        private int gyozelmekSzama;

        public int Id
        {
            get {return id; }
            set {Set( ref id , value); }
        }

        public string Nev
        {
            get { return nev; }
            set {Set(ref nev , value); }
        }
        public int AlapitasEve
        {
            get { return alapitasEve; }
            set {Set(ref alapitasEve , value); }
        }
        public string Nemzetiseg
        {
            get { return nemzetiseg; }
            set {Set(ref nemzetiseg , value); }
        }
        public string Kozpont
        {
            get { return kozpont; }
            set {Set(ref kozpont , value); }
        }
        public int LoEro
        {
            get { return loEro; }
            set {Set(ref loEro , value); }
        }
        public int GyozelmekSzama
        {
            get { return gyozelmekSzama; }
            set {Set(ref gyozelmekSzama , value); }
        }


        public void CopyFrom( MotorGyartoVM other)
        {
            if (other == null) return;
            this.Id = other.Id;
            this.Nev = other.Nev;
            this.AlapitasEve = other.AlapitasEve;
            this.Nemzetiseg = other.Nemzetiseg;
            this.Kozpont = other.Kozpont;
            this.LoEro = other.LoEro;
            this.GyozelmekSzama = other.GyozelmekSzama;
        }
    }
}
