﻿// <copyright file="ILogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyF1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyF1.Data;

    /// <summary>
    /// ILogic interface.
    /// </summary>
    public interface ILogic
    {
        /// <summary>
        /// motorgyartok get metod.
        /// </summary>
        /// <returns>motorGyartok.</returns>
        IQueryable<motorGyartok> MotorgyartokGetAll();

        /// <summary>
        /// motorgyartok get one method.
        /// </summary>
        /// <param name="id">motorgyarto id.</param>
        /// <returns>motorgyarto.</returns>
        motorGyartok MotorGyartokGetOne(int id);

        /// <summary>
        /// motorgyartok create.
        /// </summary>
        /// <param name="gyartoNev">new gyartoNev.</param>
        /// <param name="alapitasEve">new alapitasEve.</param>
        /// <param name="nemzetiseg">new nemzetiseg.</param>
        /// <param name="kozpont">new kozpont.</param>
        /// <param name="loEro">new loEro.</param>
        /// <param name="gyozelmekSzama">new gyozelmekSzama.</param>
        void MotorGyartokCreate(string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama);

        /// <summary>
        /// Motorgyartok update metod.
        /// </summary>
        /// <param name="id">gyarto id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void MotorGyartokUpdate(int id, string field, string newData);

        bool MotorGyUpdate(int id, string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama);
        /// <summary>
        /// Motorgyartok delete metod.
        /// </summary>
        /// <param name="id">Delete Motorgyartok.</param>
        void MotorGyartokDelete(int id);

        bool MotorGyDelete(int id);

        /// <summary>
        /// csapatok get metod.
        /// </summary>
        /// <returns>csapatok.</returns>
        IQueryable<csapatok> CsapatokGetAll();

        /// <summary>
        /// csapatok get one method.
        /// </summary>
        /// <param name="id">csapat id.</param>
        /// <returns>csapat.</returns>
        csapatok CsapatokGetOne(int id);

       /// <summary>
       /// csapatok create.
       /// </summary>
       /// <param name="csapatNev">new csapatNev.</param>
       /// <param name="alapitasEve">new alapitasEve.</param>
       /// <param name="kozpont">new kozpont.</param>
       /// <param name="csapatSzin">new csapatSzin.</param>
       /// <param name="gyozelmekSzama">new gyozelmekSzama.</param>
       /// <param name="bajnokiCimekSzama">int bajnokiCimekSzama.</param>
       /// <param name="gyartoID">new gyartoID.</param>
        void CsapatokCreate(string csapatNev, int alapitasEve, string kozpont, string csapatSzin, int gyozelmekSzama, int bajnokiCimekSzama, int gyartoID);

        /// <summary>
        /// csapatok update mehod.
        /// </summary>
        /// <param name="id">csapat id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void CsapatokUpdate(int id, string field, string newData);

        /// <summary>
        /// csapatok delete method.
        /// </summary>
        /// <param name="id">delete id.</param>
        void CsapatokDelete(int id);

        /// <summary>
        /// pilota get method.
        /// </summary>
        /// <returns>pilota.</returns>
        IQueryable<pilota> PilotaGetAll();

        /// <summary>
        /// pilota get one method.
        /// </summary>
        /// <param name="id">pilota id.</param>
        /// <returns>pilota.</returns>
        pilota PilotaGetOne(int id);

      /// <summary>
      /// pilota create.
      /// </summary>
      /// <param name="nev">new nev.</param>
      /// <param name="karrierKezdesEve">new karrierKezdesEve.</param>
      /// <param name="nemzetiseg">new nemzetiseg.</param>
      /// <param name="gyozelmekSzama">new gyozelmekSzama.</param>
      /// <param name="bajnokiCimekSzama">new bajnokiCimekSzama.</param>
        void PilotaCreate(string nev, int karrierKezdesEve, string nemzetiseg, int gyozelmekSzama, int bajnokiCimekSzama);

        /// <summary>
        /// Pilota update method.
        /// </summary>
        /// <param name="id">pilota id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void PilotaUpdate(int id, string field, string newData);

        /// <summary>
        /// Pilota delete method.
        /// </summary>
        /// <param name="id">delete id.</param>
        void PilotaDelete(int id);

        /// <summary>
        /// csapataXpilota get method.
        /// </summary>
        /// <returns>csapatXpilota.</returns>
        IQueryable<csapatXpilota> CsapatXpilotaGetAll();

        /// <summary>
        /// csapatxpilota get one method.
        /// </summary>
        /// <param name="id">csapatxpilota id.</param>
        /// <returns>csapatxpilota.</returns>
        csapatXpilota CsapatXpilotaGetOne(int id);

        /// <summary>
        /// csapatXpilota create.
        /// </summary>
        /// <param name="csapatID">new csapatID.</param>
        /// <param name="pilotaID">new pilotaID.</param>
        void CsapatXPilotaCreate(int csapatID, int pilotaID);

        /// <summary>
        /// csapatXpilota update method.
        /// </summary>
        /// <param name="id">cxp id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void CsapatXPilotaUpdate(int id, string field, string newData);

        /// <summary>
        /// csapatXpilota delete method.
        /// </summary>
        /// <param name="id">delete id.</param>
        void CsapatXPilotaDelete(int id);

        /// <summary>
        /// most successful pilota name.
        /// </summary>
        /// <returns>pilota name.</returns>
        string MostSuccessfulPilot();

        /// <summary>
        /// Pilots at least 4 wc and their teams where have contract.
        /// </summary>
        /// <param name="vb">number of championship wins.</param>
        /// <returns>Pilota and csaapat.</returns>
        List<string> PilotsandTeamAtLeast4Pilotwc(int vb);

        /// <summary>
        /// Number of the most constructor wins in grand prix.
        /// </summary>
        /// <returns>most wins.</returns>
        int NumberOfMostTeamWinsInGrandPrixes();

        /// <summary>
        /// Constructor of the most victorious pilot.
        /// </summary>
        /// <returns>engine constructor name.</returns>
        string ConstructorOfTheMostVictoriousPilot();
    }
}
