﻿// <copyright file="Logic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyF1.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyF1.Data;
    using MyF1.Repository;

    /// <summary>
    /// Logic.
    /// </summary>
    public class Logic : ILogic
    {
        private readonly IRepository repository;

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        public Logic()
        {
            this.repository = new Repository();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logic"/> class.
        /// </summary>
        /// <param name="repository">repo using.</param>
        public Logic(IRepository repository)
        {
            this.repository = repository;
        }

        /// <summary>
        /// csapat create.
        /// </summary>
        /// <param name="csapatNev">new csapatNev.</param>
        /// <param name="alapitasEve">alapitasEve.</param>
        /// <param name="kozpont">kozpont.</param>
        /// <param name="csapatSzin">csapatSzin.</param>
        /// <param name="gyozelmekSzama">gyozelmek szama.</param>
        /// <param name="bajnokiCimekSzama">bajnoki cimek szama.</param>
        /// <param name="gyartoID">gyarto id.</param>
        public void CsapatokCreate(string csapatNev, int alapitasEve, string kozpont, string csapatSzin, int gyozelmekSzama, int bajnokiCimekSzama, int gyartoID)
        {
            this.repository.CsapatokCreate(csapatNev, alapitasEve, kozpont, csapatSzin, gyozelmekSzama, bajnokiCimekSzama, gyartoID);
        }

        /// <summary>
        /// csapatok delete.
        /// </summary>
        /// <param name="id">csapat id.</param>
        public void CsapatokDelete(int id)
        {
            this.repository.CsapatokDelete(id);
        }

        /// <summary>
        /// return csapatok.
        /// </summary>
        /// <returns>csapatok.</returns>
        // List<csapatok>
        public IQueryable<csapatok> CsapatokGetAll()
        {
            // return this.repository.CsapatokGetAll().ToList();
            return this.repository.CsapatokGetAll();
        }

        /// <summary>
        /// csapatok get one method.
        /// </summary>
        /// <param name="id">csapat id.</param>
        /// <returns>csapat.</returns>
        public csapatok CsapatokGetOne(int id)
        {
            return this.repository.CsapatokGetOne(id);
        }

        /// <summary>
        /// csapatok update.
        /// </summary>
        /// <param name="id">csapat id.</param>
        /// <param name="field">csapat field.</param>
        /// <param name="newData">new data.</param>
        public void CsapatokUpdate(int id, string field, string newData)
        {
            this.repository.CsapatokUpdate(id, field, newData);
        }

        /// <summary>
        /// csapatXpilota create.
        /// </summary>
        /// <param name="csapatID">csapat id.</param>
        /// <param name="pilotaID">pilota id.</param>
        public void CsapatXPilotaCreate(int csapatID, int pilotaID)
        {
            this.repository.CsapatXPilotaCreate(csapatID, pilotaID);
        }

        /// <summary>
        /// csapatXpilota delete.
        /// </summary>
        /// <param name="id">csapatXpilota id.</param>
        public void CsapatXPilotaDelete(int id)
        {
            this.repository.CsapatXPilotaDelete(id);
        }

        /// <summary>
        /// return csapatXpilota.
        /// </summary>
        /// <returns>csapatXpilota.</returns>
        // List<csapatXpilota>
        public IQueryable<csapatXpilota> CsapatXpilotaGetAll()
        {
            // return this.repository.CsapatXpilotaGetAll().ToList();
            return this.repository.CsapatXpilotaGetAll();
        }

        /// <summary>
        /// csapatxpilota get one method.
        /// </summary>
        /// <param name="id">csapatxpilota id.</param>
        /// <returns>csapatxpilota.</returns>
        public csapatXpilota CsapatXpilotaGetOne(int id)
        {
            return this.repository.CsapatXpilotaGetOne(id);
        }

        /// <summary>
        /// csapatXpilota update.
        /// </summary>
        /// <param name="id">csapatXpilota id.</param>
        /// <param name="field">csapatXpilota field.</param>
        /// <param name="newData">new data.</param>
        public void CsapatXPilotaUpdate(int id, string field, string newData)
        {
            this.repository.CsapatXPilotaUpdate(id, field, newData);
        }

        /// <summary>
        /// motorgyartok create method.
        /// </summary>
        /// <param name="gyartoNev">new gyartonev.</param>
        /// <param name="alapitasEve">new alapitas eve.</param>
        /// <param name="nemzetiseg">new nemzetiseg.</param>
        /// <param name="kozpont">new kozpont.</param>
        /// <param name="loEro">new loero.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        public void MotorGyartokCreate(string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama)
        {
            this.repository.MotorGyartokCreate(gyartoNev, alapitasEve, nemzetiseg, kozpont, loEro, gyozelmekSzama);
        }

        ///// <summary>
        ///// motorGyartok create.
        ///// </summary>
        ///// <param name="gyartoNev">gyarto name.</param>
        ///// <param name="alapitasEve">alapitas eve.</param>
        ///// <param name="nemzetiseg">nemzetiseg.</param>
        ///// <param name="kozpont">kozpont.</param>
        ///// <param name="loEro">loero.</param>
        ///// <param name="gyozelmekSzama">gyozelmekszama.</param>
        // public void MotorGyartokCreate(string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama)
        // {
        //    int lastID = (int)this.repository.MotorgyartokGetAll().Select(x => x.ID).Max();
        //    motorGyartok mgy = new motorGyartok()
        //    {
        //        ID = lastID + 1,
        //        gyartoNev = gyartoNev,
        //        alapitasEve = alapitasEve,
        //        nemzetiseg = nemzetiseg,
        //        kozpont = kozpont,
        //        loEro = loEro,
        //        gyozelmekSzama = gyozelmekSzama,
        //    };
        //    this.repository.MotorGyartokCreate(mgy);
        // }

        /// <summary>
        /// motorgyartok delete.
        /// </summary>
        /// <param name="id">motorgyartok id.</param>
        public void MotorGyartokDelete(int id)
        {
            this.repository.MotorGyartokDelete(id);
        }

        public bool MotorGyDelete(int id)
        {
            return this.repository.MotorGyDelete(id);
        }
        /// <summary>
        /// return motorgyartok.
        /// </summary>
        /// <returns>motorgyartok.</returns>
        // List<motorGyartok>
        public IQueryable<motorGyartok> MotorgyartokGetAll()
        {
            // return this.repository.MotorgyartokGetAll().ToList();
            return this.repository.MotorgyartokGetAll();
        }

        /// <summary>
        /// motorgyartok get one.
        /// </summary>
        /// <param name="id">motorgyarto id.</param>
        /// <returns>motorgyarto.</returns>
        public motorGyartok MotorGyartokGetOne(int id)
        {
            return this.repository.MotorGyartokGetOne(id);
        }

        /// <summary>
        /// motorgyartok update.
        /// </summary>
        /// <param name="id">motorgyartok id.</param>
        /// <param name="field">motorgyartok field.</param>
        /// <param name="newData">new data.</param>
        public void MotorGyartokUpdate(int id, string field, string newData)
        {
            this.repository.MotorGyartokUpdate(id, field, newData);
        }
        public bool MotorGyUpdate(int id, string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama)
            {
            return this.repository.MotorGyUpdate(id,gyartoNev,alapitasEve,nemzetiseg, kozpont,loEro,gyozelmekSzama);
            }
        /// <summary>
        /// pilota create.
        /// </summary>
        /// <param name="nev">pilota name.</param>
        /// <param name="karrierKezdesEve">karrier kezdete.</param>
        /// <param name="nemzetiseg">nemzetiseg.</param>
        /// <param name="gyozelmekSzama">gyozelmek szama.</param>
        /// <param name="bajnokiCimekSzama">bajnoki cimek szama.</param>
        public void PilotaCreate(string nev, int karrierKezdesEve, string nemzetiseg, int gyozelmekSzama, int bajnokiCimekSzama)
        {
            this.repository.PilotaCreate(nev, karrierKezdesEve, nemzetiseg, gyozelmekSzama, bajnokiCimekSzama);
        }

        /// <summary>
        /// pilota delete.
        /// </summary>
        /// <param name="id">pilota id.</param>
        public void PilotaDelete(int id)
        {
            this.repository.PilotaDelete(id);
        }

        /// <summary>
        /// return pilota.
        /// </summary>
        /// <returns>pilota.</returns>
        // List<pilota>
        public IQueryable<pilota> PilotaGetAll()
        {
            // return this.repository.PilotaGetAll().ToList();
            return this.repository.PilotaGetAll();
        }

        /// <summary>
        /// pilota get one method.
        /// </summary>
        /// <param name="id">pilota id.</param>
        /// <returns>pilota.</returns>
        public pilota PilotaGetOne(int id)
        {
            return this.repository.PilotaGetOne(id);
        }

        /// <summary>
        /// pilota update.
        /// </summary>
        /// <param name="id">pilota id.</param>
        /// <param name="field">pilota field.</param>
        /// <param name="newData">new data.</param>
        public void PilotaUpdate(int id, string field, string newData)
        {
            this.repository.PilotaUpdate(id, field, newData);
        }

        /// <summary>
        /// MostSuccessfulPilot method.
        /// </summary>
        /// <returns>pilota.</returns>
        public string MostSuccessfulPilot()
        {
            var res = this.repository.PilotaGetAll().Max(x => x.bajnokiCimekSzama);
            var pil = this.repository.PilotaGetAll().First(y => y.bajnokiCimekSzama == res).nev;
            return (string)pil.ToString();
        }

        /// <summary>
        /// NumberOfMostTeamWinsInGrandPrixes method.
        /// </summary>
        /// <returns> max number of wins.</returns>
        public int NumberOfMostTeamWinsInGrandPrixes()
        {
            var res = this.repository.MotorgyartokGetAll().Max(x => x.gyozelmekSzama);
            return (int)res;
        }

        /// <summary>
        /// Constructor of the most victorious Pilota.
        /// </summary>
        /// <returns>Engine constructor name.</returns>
        public string ConstructorOfTheMostVictoriousPilot()
        {
            var maxWin = this.repository.PilotaGetAll().Max(x => x.gyozelmekSzama);
            var bestpId = this.repository.PilotaGetAll().Single(x => x.gyozelmekSzama == maxWin).pilotaID;
            var cxp = this.repository.CsapatXpilotaGetAll().Where(x => x.pilotaID == bestpId).First();
            var csap = this.repository.CsapatokGetAll().Single(x => x.ID == cxp.csapatID);
            var motorgy = this.repository.MotorgyartokGetAll().Single(x => x.ID == csap.gyartoID);

            return motorgy.gyartoNev;
        }

        /// <summary>
        /// Pilots at least 4 wc and their teams where have contract.
        /// </summary>
        /// <param name="vb">number of the worldchampions.</param>
        /// <returns>pilots and their team.</returns>
        public List<string> PilotsandTeamAtLeast4Pilotwc(int vb)
        {
            List<csapatok> csapatok = this.repository.CsapatokGetAll().ToList();
            List<csapatXpilota> cxp = this.repository.CsapatXpilotaGetAll().ToList();
            List<pilota> pilotak = this.repository.PilotaGetAll().ToList();

            var res = from x in pilotak
                      join y in cxp on x.pilotaID equals y.pilotaID
                      join z in csapatok on y.csapatID equals z.ID
                      where x.bajnokiCimekSzama > vb
                      select new
                      {
                          Nev = x.nev,
                          Csapat = z.csapatNev,
                      }.ToString();
            return res.ToList();
        }
    }
}