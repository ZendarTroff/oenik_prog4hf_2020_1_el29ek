﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyF1.Program
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Xml.Linq;
    using MyF1.Logic;

    /// <summary>
    /// Program exe.
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Main.
        /// </summary>
        /// <param name="args">args parameter.</param>
        public static void Main(string[] args)
        {
            MainMenu();
        }

        /// <summary>
        /// Main Menu.
        /// </summary>
        public static void MainMenu()
        {
            List<string> mainMenu = new List<string>() { "1 Listázás", "2 Keresett ID kiírása", "3 Hozzáadás", "4 Módosítás", "5 Törlés", "6 JAVA", "7 Kilépés" };
            foreach (string item in mainMenu)
            {
                Console.WriteLine(item);
            }

            Console.Write("Kérlek válasz a fenti menükből!\n");

            int menuPont = 0;
            while (menuPont != 7)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        ListMenu();
                        break;
                    case 2:
                        ByIDMenu();
                        break;
                    case 3:
                        InsertMenu();
                        break;
                    case 4:
                        UpdateMenu();
                        break;
                    case 5:
                        DeleteMenu();
                        break;
                    case 6:
                        Java();
                        break;
                    case 7:
                        Environment.Exit(0);
                        break;
                    default:
                        break;
                }
            }
        }

        private static void ByIDMenu()
        {
            Console.Clear();
            List<string> listMenu = new List<string>() { "1 Motorgyártok", "2 Csapatok", "3 Piloták", "4 Csapat-Pilota kapcsoló", "5 Visszalépés a főmenübe" };

            Logic logic = new Logic();
            foreach (string item in listMenu)
            {
                Console.WriteLine(item);
            }

            Console.Write("Melyik táblába keresed az ID-t?\n");
            int menuPont = 0;
            while (menuPont != 6)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        Console.WriteLine("Ad meg a keresett ID-t");
                        int m = int.Parse(Console.ReadLine());
                        Console.Clear();
                        var motorGyartok = logic.MotorGyartokGetOne(m);
                        Console.WriteLine("ID" + "\t" + "Gyarto" + "\t" + "Alapítás éve" + "\t" + "Nemzetiség" + "\t" + "Központ" + "\t" + "Lóerő" + "\t" + "Győzelmek száma");
                        Console.WriteLine(motorGyartok.ID + "\t" + motorGyartok.gyartoNev + "\t" + motorGyartok.alapitasEve + "\t" + motorGyartok.nemzetiseg + "\t" + motorGyartok.kozpont + "\t" + motorGyartok.loEro + "\t" + motorGyartok.gyozelmekSzama);
                        Thread.Sleep(3000);
                        ByIDMenu();
                        break;
                    case 2:
                        Console.WriteLine("Ad meg a keresett ID-t");
                        int cs = int.Parse(Console.ReadLine());
                        Console.Clear();
                        var csapatok = logic.CsapatokGetOne(cs);
                        Console.WriteLine("ID" + "\t" + "csapat Neve" + "\t" + "alapítás éve" + "\t" + "központ" + "\t" + "Csapat Szín" + "\t" + "Győzelmek száma" + "\t" + "Bajnoki címek száma" + "\t" + "GyártóID");
                        Console.WriteLine(csapatok.ID + "\t" + csapatok.csapatNev + "\t" + csapatok.alapitasEve + "\t" + csapatok.kozpont + "\t" + csapatok.csapatSzin + "\t" + csapatok.gyozelmekSzama + "\t" + csapatok.bajnokiCimekSzama + "\t" + csapatok.gyartoID);
                        Thread.Sleep(3000);
                        ByIDMenu();
                        break;
                    case 3:
                        Console.WriteLine("Ad meg a keresett ID-t");
                        int p = int.Parse(Console.ReadLine());
                        Console.Clear();
                        var pilotak = logic.PilotaGetOne(p);
                        Console.WriteLine("ID" + "\t" + "Név" + "\t" + "karrier kezdete" + "\t" + "nemzetiség" + "\t" + "győzelmek Száma" + "\t" + "Bajnoki címek száma");
                        Console.WriteLine(pilotak.pilotaID + "\t" + pilotak.nev + "\t" + pilotak.karrierKezdesEve + "\t" + pilotak.nemzetiseg + "\t" + pilotak.gyozelmekSzama + "\t" + pilotak.bajnokiCimekSzama);
                        Thread.Sleep(3000);
                        ByIDMenu();
                        break;
                    case 4:
                        Console.WriteLine("Ad meg a keresett ID-t");
                        int cxp = int.Parse(Console.ReadLine());
                        Console.Clear();
                        var szerzodesek = logic.CsapatXpilotaGetOne(cxp);
                        Console.WriteLine("SzerződésID" + "\t" + "csapatID" + "\t" + "pilotaID");
                        Console.WriteLine("\t" + szerzodesek.szerzodesID + "\t" + szerzodesek.csapatID + "\t\t" + szerzodesek.pilotaID);
                        Thread.Sleep(3000);
                        ByIDMenu();
                        break;
                    case 5:
                        MainMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void ListMenu()
        {
            List<string> listMenu = new List<string>() { "1 Motorgyártok", "2 Csapatok", "3 Piloták", "4 Csapat-Pilota kapcsoló", "5 NEM CRUD lekérdezések", "6 Visszalépés a főmenübe" };
            Logic logic = new Logic();
            foreach (string item in listMenu)
            {
                Console.WriteLine(item);
            }

            Console.Write("Kérlek válasz a fenti menükből!\n");

            int menuPont = 0;
            while (menuPont != 7)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        var motorGyartok = logic.MotorgyartokGetAll();
                        Console.WriteLine("ID" + "\t" + "Gyarto" + "\t" + "Alapítás éve" + "\t" + "Nemzetiség" + "\t" + "Központ" + "\t" + "Lóerő" + "\t" + "Győzelmek száma");
                        foreach (var item in motorGyartok)
                        {
                            Console.WriteLine(item.ID + "\t" + item.gyartoNev + "\t\t" + item.alapitasEve + "\t\t" + item.nemzetiseg + "\t" + item.kozpont + "\t" + item.loEro + "\t" + item.gyozelmekSzama);

                            // Console.WriteLine("{0}  \t  {1}  \t  {2}  \t  {3}  \t  {4}  \t  {5}", item.ID, item.gyartoNev, item.alapitasEve, item.nemzetiseg, item.loEro, item.gyozelmekSzama);
                        }

                        break;
                    case 2:
                        var csapatok = logic.CsapatokGetAll();
                        Console.WriteLine("CsapatID" + "\t" + "csapat Neve" + "\t" + "alapítás éve" + "\t" + "központ" + "\t" + "Csapat Szín" + "\t" + "Győzelmek száma" + "\t" + "Bajnoki címek száma" + "\t" + "GyártóID");
                        foreach (var item in csapatok)
                        {
                            Console.WriteLine(item.ID + "\t" + item.csapatNev + "\t" + item.alapitasEve + "\t" + item.kozpont + "\t" + item.csapatSzin + "\t" + item.gyozelmekSzama + "\t" + item.bajnokiCimekSzama + "\t" + item.gyartoID);
                        }

                        break;
                    case 3:
                        var pilotak = logic.PilotaGetAll();
                        Console.WriteLine("pilotaID" + "\t" + "Név" + "\t" + "karrier kezdete" + "\t" + "nemzetiség" + "\t" + "győzelmek Száma" + "\t" + "Bajnoki címek száma");
                        foreach (var item in pilotak)
                        {
                            Console.WriteLine(item.pilotaID + "\t" + item.nev + "\t" + item.karrierKezdesEve + "\t" + item.nemzetiseg + "\t" + item.gyozelmekSzama + "\t" + item.bajnokiCimekSzama);
                        }

                        break;
                    case 4:
                        var szerzodesek = logic.CsapatXpilotaGetAll();
                        Console.WriteLine("SzerződésID" + "\t" + "csapatID" + "\t" + "pilotaID");
                        foreach (var item in szerzodesek)
                        {
                            Console.WriteLine("\t" + item.szerzodesID + "\t" + item.csapatID + "\t\t" + item.pilotaID);
                        }

                        break;
                    case 5:
                        NonCrudMenu();
                        break;
                    case 6:
                        MainMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void NonCrudMenu()
        {
            List<string> nonCrudListMenu = new List<string>() { "1 A legsikeresebb versenyző", "2 A legtöbb győzelmet szerő csapat", "3 A legtöbb futamgyőzelmet szerző pilota motorgyártója", "4 Legalább 4 VB címmel rendelkező piloták is csapataik", "5 Visszalépés a Listákhoz" };
            Console.Clear();
            foreach (string item in nonCrudListMenu)
            {
                Console.WriteLine(item);
            }

            Console.Write("Kérlek válasz a fenti lehetőségekből!\n");

            int menuPont = 0;
            while (menuPont != 5)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        MostSuccessfulPilot();
                        break;
                    case 2:
                        NumberOfMostTeamWinsInGrandPrixes();
                        break;
                    case 3:
                        ConstructorOfTheMostVictoriousPilot();
                        break;
                    case 4:
                        PilotsandTeamAtLeast4Pilotwc();
                        break;
                    case 5:
                        ListMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void ConstructorOfTheMostVictoriousPilot()
        {
            Logic logic = new Logic();

            Console.WriteLine(" A legtöbb győzelmet szerző pilota konstruktőre: " + logic.ConstructorOfTheMostVictoriousPilot());
            Console.WriteLine("Visszatérés a non-crud menübe 5 sec múlva!");
            Thread.Sleep(5000);
            Console.Clear();
            NonCrudMenu();
        }

        private static void NumberOfMostTeamWinsInGrandPrixes()
        {
            Logic logic = new Logic();

            Console.WriteLine("A legtöbb futamgyőzelem: " + logic.NumberOfMostTeamWinsInGrandPrixes());
            Console.WriteLine("Visszatérés a non-crud menübe 5 sec múlva!");
            Thread.Sleep(5000);
            Console.Clear();
            NonCrudMenu();
        }

        private static void MostSuccessfulPilot()
        {
            Logic logic = new Logic();

            Console.WriteLine("A legsikeresebb versenyző: " + logic.MostSuccessfulPilot());
            Console.WriteLine("Visszatérés a non-crud menübe 5 sec múlva!");
            Thread.Sleep(5000);
            Console.Clear();
            NonCrudMenu();
        }

        private static void PilotsandTeamAtLeast4Pilotwc()
        {
            Logic logic = new Logic();

            List<string> list = logic.PilotsandTeamAtLeast4Pilotwc(4);
            Console.WriteLine("A 4 VB címet szerző piloták:\n");
            foreach (var item in list)
            {
                Console.WriteLine(item.TrimStart('{').TrimEnd('}'));
            }

            Thread.Sleep(5000);
            Console.Clear();
            NonCrudMenu();
        }

        /// <summary>
        /// Insert menu.
        /// </summary>
        private static void InsertMenu()
        {
            Console.WriteLine("Melyik táblához szeretnél hozzá adni?\n");
            List<string> tableList = new List<string>() { "1 Motorgyártok", "2 Csapatok", "3 Piloták", "4 Csapat-Pilota kapcsoló", "5 Visszalépés a főmenűbe" };

            foreach (string item in tableList)
            {
                Console.WriteLine(item);
            }

            int menuPont = 0;
            while (menuPont != 6)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        InsertMotorGyartok();
                        break;
                    case 2:
                        InsertCsapatok();
                        break;
                    case 3:
                        InsertPilotak();
                        break;
                    case 4:
                        InsertCsapatXPilota();
                        break;
                    case 5:
                        MainMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void InsertCsapatXPilota()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a Csapat ID-t! maximálisan megadható {0}", logic.CsapatokGetAll().Count());
            int csapatID = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a Pilota ID-t!  maximálisan megadható {0}", logic.PilotaGetAll().Count());
            int pilotaID = int.Parse(Console.ReadLine());
            logic.CsapatXPilotaCreate(csapatID, pilotaID);
            Console.WriteLine("Hozzáadva! Visszatérés a főmenűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            MainMenu();
        }

        private static void InsertPilotak()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a Nevet!");
            string nev = Console.ReadLine();
            Console.WriteLine("Ad meg a karrierjének kezdetét!(évszám)");
            int karrierkezdete = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a nemzetiségét!");
            string nemzetiseg = Console.ReadLine();
            Console.WriteLine("Ad meg a győzelmei számát!");
            int gyozelmekSzama = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a bajnoki címei számát!");
            int vbCímekSzama = int.Parse(Console.ReadLine());
            logic.PilotaCreate(nev, karrierkezdete, nemzetiseg, gyozelmekSzama, vbCímekSzama);
            Console.WriteLine("Hozzáadva! Visszatérés a főmenűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            MainMenu();
        }

        private static void InsertCsapatok()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a csapat nevét!");
            string csapatnev = Console.ReadLine();
            Console.WriteLine("Ad meg az alapításának évét!");
            int alapitasEve = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a központ helyét!");
            string kozpont = Console.ReadLine();
            Console.WriteLine("Ad meg a csapat színét!");
            string csapatSzin = Console.ReadLine();
            Console.WriteLine("Ad meg a csapat győzelmeinek számát!");
            int gyozelmekSzama = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a csapat bajnoki címei számát!");
            int vbCímekSzama = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a motorgyártó ID-ját! maximálisan megadható {0}", logic.MotorgyartokGetAll().Count());
            int gyartoID = int.Parse(Console.ReadLine());
            logic.CsapatokCreate(csapatnev, alapitasEve, kozpont, csapatSzin, gyozelmekSzama, vbCímekSzama, gyartoID);
            Console.WriteLine("Hozzáadva! Visszatérés a főmenűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            MainMenu();
        }

        private static void InsertMotorGyartok()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a gyártó nevét!");
            string gyartoNev = Console.ReadLine();
            Console.WriteLine("Ad meg az alapításának évét!");
            int alapitasEve = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a gyártó nemzetiségét!");
            string nemzetiseg = Console.ReadLine();
            Console.WriteLine("Ad meg a gyár központját!");
            string kozpont = Console.ReadLine();
            Console.WriteLine("Ad meg a motor lóerejét!");
            int loEro = int.Parse(Console.ReadLine());
            Console.WriteLine("Ad meg a győzelmek számát!");
            int gyozelmekSzama = int.Parse(Console.ReadLine());
            logic.MotorGyartokCreate(gyartoNev, alapitasEve, nemzetiseg, kozpont, loEro, gyozelmekSzama);
            Console.WriteLine("Hozzáadva! Visszatérés a főmenűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            MainMenu();
        }

        private static void UpdateMenu()
        {
            Console.WriteLine("Melyik táblában szeretnél frissíteni?\n");
            List<string> tableList = new List<string>() { "1 Motorgyártok", "2 Csapatok", "3 Piloták", "4 Csapat-Pilota kapcsoló", "5 Visszalépés a főmenűbe" };

            foreach (string item in tableList)
            {
                Console.WriteLine(item);
            }

            int menuPont = 0;
            while (menuPont != 6)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        UpdateMotorGyartok();
                        break;
                    case 2:
                        UpdateCsapatok();
                        break;
                    case 3:
                        UpdatePilotak();
                        break;
                    case 4:
                        UpdateCsapatXPilota();
                        break;
                    case 5:
                        MainMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void UpdateCsapatXPilota()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a szerződés ID-t amit megváltoztatnál");
            int updateID = int.Parse(Console.ReadLine());
            Console.WriteLine("Milyen adatot változtatnál meg benne?");
            Console.WriteLine("1 csapatID  \n2 pilotaID  \n3 Visszalépés");
            int fieldID = int.Parse(Console.ReadLine());
            switch (fieldID)
            {
                case 1:
                    Console.WriteLine("Mi legyen az új csapatID?");
                    string csapatID = Console.ReadLine();
                    logic.CsapatXPilotaUpdate(updateID, "csapatID", csapatID);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 2:
                    Console.WriteLine("Mi legyen az új pilotaID?");
                    string pilotaID = Console.ReadLine();
                    logic.CsapatXPilotaUpdate(updateID, "pilotaID", pilotaID);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 3:
                    Console.Clear();
                    UpdateMenu();
                    break;
                default:
                    break;
            }
        }

        private static void UpdatePilotak()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a pilota ID-t amit megváltoztatnál");
            int updateID = int.Parse(Console.ReadLine());
            Console.WriteLine("Milyen adatot változtatnál meg benne?");
            Console.WriteLine("1 Név \n2 karrierkezdés éve \n3 nemzetiség \n4 győzelmek száma \n5 bajnoki címek száma \n6 Visszalépés");
            int fieldID = int.Parse(Console.ReadLine());
            switch (fieldID)
            {
                case 1:
                    Console.WriteLine("Ad meg a nevet!");
                    string nev = Console.ReadLine();
                    logic.PilotaUpdate(updateID, "nev", nev);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 2:
                    Console.WriteLine("Ad meg a karrier kezdésének évét!");
                    string kezdeseve = Console.ReadLine();
                    logic.PilotaUpdate(updateID, "karrierKezdesEve", kezdeseve);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 3:
                    Console.WriteLine("Ad meg a nemzetiségét!");
                    string nemzetiseg = Console.ReadLine();
                    logic.PilotaUpdate(updateID, "nemzetiseg", nemzetiseg);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 4:
                    Console.WriteLine("Ad meg a győzelmeinek számát!");
                    string gyozelmekSzama = Console.ReadLine();
                    logic.PilotaUpdate(updateID, "gyozelmekSzama", gyozelmekSzama);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 5:
                    Console.WriteLine("Ad meg a bajnoki címei számát!");
                    string bajnokiCimekSzama = Console.ReadLine();
                    logic.PilotaUpdate(updateID, "bajnokiCimekSzama", bajnokiCimekSzama);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 6:
                    UpdateMenu();
                    break;
                default:
                    break;
            }
        }

        private static void UpdateCsapatok()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a csapat ID-t amit megváltoztatnál");
            int updateID = int.Parse(Console.ReadLine());
            Console.WriteLine("Milyen adatot változtatnál meg benne?");
            Console.WriteLine("1 Csapat név \n2 alapítás éve \n3 központ helye \n4 csapat színe \n5 győzelmek száma \n6 bajnoki címek száma \n7 motorgyártóID \n8 Visszalépés");
            int fieldID = int.Parse(Console.ReadLine());
            switch (fieldID)
            {
                case 1:
                    Console.WriteLine("Ad meg a nevet!");
                    string nev = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "csapatNev", nev);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 2:
                    Console.WriteLine("Ad meg az alapítás évét!");
                    string alapitasEve = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "alaptiasEve", alapitasEve);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 3:
                    Console.WriteLine("Ad meg a központ helyét!");
                    string kozpont = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "kozpont", kozpont);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 4:
                    Console.WriteLine("Ad meg a csapat színét!");
                    string csapatSzin = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "csapatSzin", csapatSzin);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 5:
                    Console.WriteLine("Ad meg a győzelmek számát!");
                    string gyozelmekSzama = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "gyozelmekSzama", gyozelmekSzama);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 6:
                    Console.WriteLine("Ad meg a bajnoki címek számát!");
                    string bajnokiCimekSzama = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "bajnokiCimekSzama", bajnokiCimekSzama);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 7:
                    Console.WriteLine("Ad meg a motorgyártó ID-t!");
                    string gyartoID = Console.ReadLine();
                    logic.CsapatokUpdate(updateID, "gyartoID", gyartoID);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 8:
                    UpdateMenu();
                    break;
                default:
                    break;
            }
        }

        private static void UpdateMotorGyartok()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a motorgyártó ID-t amit megváltoztatnál");
            int updateID = int.Parse(Console.ReadLine());
            Console.WriteLine("Milyen adatot változtatnál meg benne?");
            Console.WriteLine("1 Gyártó neve \n2 Alapítás éve \n3 Nemzetiság \n4 Központ \n5 Lóerő \n6 Győzelmek száma \n7 Visszalépés");
            int fieldID = int.Parse(Console.ReadLine());
            switch (fieldID)
            {
                case 1:
                    Console.WriteLine("Ad meg a nevet!");
                    string nev = Console.ReadLine();
                    logic.MotorGyartokUpdate(updateID, "gyartoNev", nev);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 2:
                    Console.WriteLine("Ad meg az alapítás évét!");
                    string alapitasEve = Console.ReadLine();
                    logic.MotorGyartokUpdate(updateID, "alapitasEve", alapitasEve);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 3:
                    Console.WriteLine("Ad meg a nemzetiséget!");
                    string nemzetiseg = Console.ReadLine();
                    logic.MotorGyartokUpdate(updateID, "nemzetiseg", nemzetiseg);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 4:
                    Console.WriteLine("Ad meg a központ helyét!");
                    string kozpont = Console.ReadLine();
                    logic.MotorGyartokUpdate(updateID, "kozpont", kozpont);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 5:
                    Console.WriteLine("Ad meg a motor lóerejét!");
                    string loEro = Console.ReadLine();
                    logic.MotorGyartokUpdate(updateID, "loEro", loEro);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 6:
                    Console.WriteLine("Ad meg a győzelmek számát!");
                    string gyozelmekSzama = Console.ReadLine();
                    logic.MotorGyartokUpdate(updateID, "gyozelmekSzama", gyozelmekSzama);
                    Console.WriteLine("Frissítve! Visszatérés az Update menűbe 5sec múlva...");
                    Thread.Sleep(5000);
                    Console.Clear();
                    UpdateMenu();
                    break;
                case 7:
                    UpdateMenu();
                    break;
                default:
                    break;
            }
        }

        private static void DeleteMenu()
        {
            Console.WriteLine("Melyik táblából szeretnél törölni?\n");
            List<string> tableList = new List<string>() { "1 Motorgyártok", "2 Csapatok", "3 Piloták", "4 Csapat-Pilota kapcsoló", "5 Visszalépés a főmenűbe" };

            foreach (string item in tableList)
            {
                Console.WriteLine(item);
            }

            int menuPont = 0;
            while (menuPont != 6)
            {
                menuPont = int.Parse(Console.ReadLine());
                Console.Clear();
                switch (menuPont)
                {
                    case 1:
                        DeleteMotorGyartok();
                        break;
                    case 2:
                        DeleteCsapatok();
                        break;
                    case 3:
                        DeletePilotak();
                        break;
                    case 4:
                        DeleteCsapatXPilota();
                        break;
                    case 5:
                        MainMenu();
                        break;
                    default:
                        break;
                }
            }
        }

        private static void DeleteMotorGyartok()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a motorgyártó ID-t amit törölnél");
            int deleteID = int.Parse(Console.ReadLine());
            var csapatok = logic.CsapatokGetAll();
            var csapatXpilota = logic.CsapatXpilotaGetAll();

            foreach (var csapat in csapatok)
            {
                if (csapat.gyartoID == deleteID)
                {
                    foreach (var cxp in csapatXpilota)
                    {
                        if (cxp.csapatID == csapat.ID)
                        {
                            logic.CsapatXPilotaDelete((int)cxp.szerzodesID);
                        }
                    }

                    logic.CsapatokDelete((int)csapat.ID);
                }
            }

            logic.MotorGyartokDelete(deleteID);
            Console.WriteLine("Törölve! Visszatérés az Delete menűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            DeleteMenu();
        }

        private static void DeleteCsapatok()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a csapat ID-t amit törölnél");
            int deleteID = int.Parse(Console.ReadLine());
            var csapatXpilota = logic.CsapatXpilotaGetAll();
            foreach (var item in csapatXpilota)
            {
                if (item.csapatID == deleteID)
                {
                    logic.CsapatXPilotaDelete((int)item.szerzodesID);
                }
            }

            logic.CsapatokDelete(deleteID);
            Console.WriteLine("Törölve! Visszatérés az Delete menűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            DeleteMenu();
        }

        private static void DeletePilotak()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a pilota ID-t amit törölnél");
            int deleteID = int.Parse(Console.ReadLine());
            var csapatXpilota = logic.CsapatXpilotaGetAll();
            foreach (var item in csapatXpilota)
            {
                if (item.pilotaID == deleteID)
                {
                    logic.CsapatXPilotaDelete((int)item.szerzodesID);
                }
            }

            logic.PilotaDelete(deleteID);
            Console.WriteLine("Törölve! Visszatérés az Delete menűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            DeleteMenu();
        }

        private static void DeleteCsapatXPilota()
        {
            Logic logic = new Logic();
            Console.WriteLine("Ad meg a szerződés ID-t amit törölnél");
            int deleteID = int.Parse(Console.ReadLine());
            logic.CsapatXPilotaDelete(deleteID);
            Console.WriteLine("Törölve! Visszatérés az Delete menűbe 5sec múlva...");
            Thread.Sleep(5000);
            Console.Clear();
            DeleteMenu();
        }

        private static void Java()
        {
            Logic logic = new Logic();
            Console.WriteLine("Adj meg egy motorgyártót!");
            string gyarto = Console.ReadLine();
            var loErok = logic.MotorgyartokGetAll();
            int atlag = 0;
            foreach (var item in loErok)
            {
                atlag += (int)item.loEro;
            }

            string loEro = (atlag / loErok.Count()).ToString();

            // string loEro = "400";
            XDocument xdoc = XDocument.Load(@"http://localhost:8080/MyF1.JavaWeb/DataServlet?motorGyartoName=" + gyarto + "&loEro=" + loEro);
            Console.WriteLine("Tulajdonos      Gyártónév    lóerő");
            foreach (var item in xdoc.Element("generatedData").Elements("newConstructors"))
            {
                Console.WriteLine(item.Element("tulajdonos").Value + "        " + item.Element("motorGyartoName").Value + "       " + item.Element("loEro").Value);
            }

            Console.ReadLine();
        }
    }
}
