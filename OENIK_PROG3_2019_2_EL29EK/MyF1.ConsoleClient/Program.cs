﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;

namespace MyF1.ConsoleClient
{
    public class MotorGyartok
    {
        public int Id { get; set; }
        public string Nev { get; set; }
        public int AlapitasEve { get; set; }
        public string Nemzetiseg { get; set; }
        public string Kozpont { get; set; }
        public int LoEro { get; set; }
        public int GyozelmekSzama { get; set; }

        public override string ToString()
        {
            return $"Id= {Id}\tNev= {Nev}\t" +
                $"AlapitasEve= {AlapitasEve}\t" +
                $"Nemzetiseg= {Nemzetiseg}\tKozpont= {Kozpont}\t" +
                $"LoEro= {LoEro}\tGyozelmekSzama= {GyozelmekSzama}\t";
        }

    }
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Waiting....");
            Console.ReadLine();

            string url = "http://localhost:7223/api/MotorGyartokApi/";

            using (HttpClient client = new HttpClient())
            {
                string json = client.GetStringAsync(url + "all").Result;
                var list= JsonConvert.DeserializeObject<List<MotorGyartok>>(json);
                foreach (var item in list) Console.WriteLine(item);
                Console.ReadLine();

                Dictionary<string, string> postData;
                string response;

                postData = new Dictionary<string, string>();
                postData.Add(nameof(MotorGyartok.Nev), "Lada");
                postData.Add(nameof(MotorGyartok.AlapitasEve), "1600");
                postData.Add(nameof(MotorGyartok.Nemzetiseg), "PL");
                postData.Add(nameof(MotorGyartok.Kozpont), "Prezli");
                postData.Add(nameof(MotorGyartok.LoEro), "50");
                postData.Add(nameof(MotorGyartok.GyozelmekSzama), "33");
                response = client.PostAsync(url + "add", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Add: "+ response);
                Console.WriteLine("All: "+json);
                Console.ReadLine();

                int motoGyId = JsonConvert.DeserializeObject<List<MotorGyartok>>(json).Single(x => x.Nev == "Lada").Id;
                postData = new Dictionary<string, string>();
                postData.Add(nameof(MotorGyartok.Id), motoGyId.ToString());
                postData.Add(nameof(MotorGyartok.Nev), "Lada");
                postData.Add(nameof(MotorGyartok.AlapitasEve), "1600");
                postData.Add(nameof(MotorGyartok.Nemzetiseg), "PL");
                postData.Add(nameof(MotorGyartok.Kozpont), "Prezli");
                postData.Add(nameof(MotorGyartok.LoEro), "100");
                postData.Add(nameof(MotorGyartok.GyozelmekSzama), "33");

                response = client.PostAsync(url + "mod", new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;

                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Mod: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();

                response = client.GetStringAsync(url + "del/"+motoGyId).Result;
                json = client.GetStringAsync(url + "all").Result;
                Console.WriteLine("Del: " + response);
                Console.WriteLine("All: " + json);
                Console.ReadLine();
            }
        }
    }
}
