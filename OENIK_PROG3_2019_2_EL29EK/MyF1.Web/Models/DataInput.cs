﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyF1.Web.Models
{
    public class DataInput
    {
        public string Nev { get; set; }
        public double TestSuly { get; set; }
        public string Gyakorlat { get; set; }
        public double Perc { get; set; }
    }
}