﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyF1.Web.Models
{
    public class MotorGyartokViewModel
    {
        public MotorGyartok EditedMGy { get; set; }
        public List<MotorGyartok> ListOfMGy { get; set; }
    }
}