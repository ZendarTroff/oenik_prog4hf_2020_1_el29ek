﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyF1.Web.Models
{
    public class MapperFactory
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<MyF1.Data.motorGyartok, MyF1.Web.Models.MotorGyartok>().
                ForMember(dest => dest.Id, map => map.MapFrom(src => src.ID)).
                ForMember(dest => dest.Nev, map => map.MapFrom(src => src.gyartoNev)).
                ForMember(dest => dest.AlapitasEve, map => map.MapFrom(src => src.alapitasEve)).
                ForMember(dest => dest.Nemzetiseg, map => map.MapFrom(src => src.nemzetiseg)).
                ForMember(dest => dest.Kozpont, map => map.MapFrom(src => src.kozpont)).
                ForMember(dest => dest.LoEro, map => map.MapFrom(src => src.loEro)).
                ForMember(dest => dest.GyozelmekSzama, map => map.MapFrom(src => src.gyozelmekSzama));
            });
            return config.CreateMapper();
        }
    }
}