﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MyF1.Web.Models
{
    //Form Model
    public class MotorGyartok
    {
        [Display(Name ="Mgy Id")]
        [Required]
        public int Id { get; set; }
        [Display(Name = "Mgy Nev")]
        [Required]
        public string Nev { get; set; }
        [Display(Name = "Mgy AlapitasEve")]
        [Required]
        public int AlapitasEve { get; set; }
        [Display(Name = "Mgy Nemzetiseg")]
        [Required]
        public string Nemzetiseg { get; set; }
        [Display(Name = "Mgy Kozpont")]
        [Required]
        public string Kozpont { get; set; }
        [Display(Name = "Mgy LoEro")]
        [Required]
        public int LoEro { get; set; }
        [Display(Name = "Mgy GyozelmekSzama")]
        [Required]
        public int GyozelmekSzama { get; set; }
    }
}