﻿using AutoMapper;
using MyF1.Data;
using MyF1.Logic;
using MyF1.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MyF1.Web.Controllers
{
    public class MotorGyartokApiController : ApiController
    {
        public class ApiResult
        {
            public bool OperationResult { get; set; }
        }

        ILogic logic;
        IMapper mapper;

        public MotorGyartokApiController()
        {
            AdatbazisEntities ctx = new AdatbazisEntities();
            Repository.Repository repo = new Repository.Repository(ctx);
            logic = new Logic.Logic(repo);
            mapper = MapperFactory.CreateMapper();
        }
        //GET api/MotorGyartokApi
        [ActionName("all")]
        [HttpGet]
        //GET api/MotorGyartokApi/all
        public IEnumerable<Models.MotorGyartok> GetAll()
        {
            var motgy = logic.MotorgyartokGetAll();
            return mapper.Map<IQueryable<Data.motorGyartok>, List<Models.MotorGyartok>>(motgy);
        }
        //GET api/MotorGyartokApi/del
        [ActionName("del")]
        [HttpGet]
        public ApiResult DelOneMotorGyarto(int id)
        {
            bool siker = logic.MotorGyDelete(id);
            return new ApiResult() { OperationResult = siker };
        }
        //POST api/MotorGyartokApi/add
        [ActionName("add")]
        [HttpPost]
        public ApiResult AddOneMotorGyarto(MotorGyartok mgy)
        {
            logic.MotorGyartokCreate(mgy.Nev, mgy.AlapitasEve, mgy.Nemzetiseg, mgy.Kozpont, mgy.LoEro, mgy.GyozelmekSzama);
            return new ApiResult() { OperationResult = true };
        }
        //POST api/MotorGyartokApi/mod
        [ActionName("mod")]
        [HttpPost]
        public ApiResult ModOneMotorGyarto(MotorGyartok mgy)
        {
            bool siker = logic.MotorGyUpdate(mgy.Id, mgy.Nev, mgy.AlapitasEve, mgy.Nemzetiseg, mgy.Kozpont, mgy.LoEro, mgy.GyozelmekSzama);
            return new ApiResult() { OperationResult = siker };
        }
    }
}
