﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MyF1.Web.Models;

namespace MyF1.Web.Controllers
{
    public class CalorieCounterController : Controller
    {
        // GET: CalorieCounter
        public ActionResult Index()
        {
            return View();
        }
        //GET: /CalorieCounter/Data
        public ActionResult Data()
        {
            return View("DataInput");
        }
        //POST: /CalorieCounter/Data
        //[HttpPost]
        //public ActionResult Data(string nev , double testSuly , string gyakorlat ,double perc)
        //{
        //    double gyakorlatKaloria = 0;
        //    double tenylegesIdo = perc / 60;
        //    double sulyKorrekcio = testSuly / 100;

        //    switch (gyakorlat)
        //    {
        //        case "Running":
        //            gyakorlatKaloria = 1000;
        //            break;
        //        case "Yoga":
        //            gyakorlatKaloria = 400;
        //            break;
        //        case "Pilates":
        //            gyakorlatKaloria = 472;
        //            break;
        //        case "Hiking":
        //            gyakorlatKaloria = 700;
        //            break;
        //        case "Swimming":
        //            gyakorlatKaloria = 1000;
        //            break;
        //        case "Bicycle":
        //            gyakorlatKaloria = 600;
        //            break;
        //        default:
        //            break;
        //    }
        //    double kalkulaltKaloria = tenylegesIdo * sulyKorrekcio * gyakorlatKaloria;

        //    Result result = new Result()
        //    {
        //        OriginalNev = nev,
        //        OriginalTestSuly = testSuly,
        //        OriginalPerc = perc,
        //    ElegetettKaloria = kalkulaltKaloria,
        //    };
        //    return View("Result", result);
        //}

        //POST: /CalorieCounter/Data
        [HttpPost]
        public ActionResult Data(DataInput input)
        {

            if (input.Nev == null)
            {
                TempData["warning"] = "INVALID DATA - Nem adtál meg szükséges adatokat!";
                return this.RedirectToAction(nameof(Data));
            }

            if (input.TestSuly < 1 || input.Perc < 1)
            {
                TempData["warning"] = "INVALID DATA - Csak 0-nál nagyobb szám adható meg!";
                return this.RedirectToAction(nameof(Data));
            }

            double gyakorlatKaloria = 0;
            double tenylegesIdo = input.Perc / 60;
            double sulyKorrekcio = input.TestSuly / 100;

            switch (input.Gyakorlat)
            {
                case "Running":
                    gyakorlatKaloria = 1000;
                    break;
                case "Yoga":
                    gyakorlatKaloria = 400;
                    break;
                case "Pilates":
                    gyakorlatKaloria = 472;
                    break;
                case "Hiking":
                    gyakorlatKaloria = 700;
                    break;
                case "Swimming":
                    gyakorlatKaloria = 1000;
                    break;
                case "Bicycle":
                    gyakorlatKaloria = 600;
                    break;
                default:
                    TempData["warning"] = "INVALID DATA - Nincs ilyen gyakorlat";
                    return this.RedirectToAction(nameof(Data));

            }
            double kalkulaltKaloria = tenylegesIdo * sulyKorrekcio * gyakorlatKaloria;



            Result result = new Result()
            {
                OriginalNev = input.Nev,
                OriginalTestSuly = input.TestSuly,
                OriginalPerc = input.Perc,
                ElegetettKaloria = kalkulaltKaloria,
            };
            return View("Result", result);
        }
    }
}