﻿using AutoMapper;
using MyF1.Data;
using MyF1.Logic;
using MyF1.Repository;
using MyF1.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MyF1.Web.Controllers
{
    public class MotorGyartokController : Controller
    {
        ILogic logic;
        IMapper mapper;
        MotorGyartokViewModel vm;

        public MotorGyartokController()
        {
            AdatbazisEntities ctx = new AdatbazisEntities();
            Repository.Repository repo = new Repository.Repository(ctx);
            logic = new Logic.Logic(repo);
            mapper = MapperFactory.CreateMapper();
            vm = new MotorGyartokViewModel();
            vm.EditedMGy = new MotorGyartok();
            var motogy = logic.MotorgyartokGetAll();
            vm.ListOfMGy = mapper.Map<IQueryable<Data.motorGyartok>,List<Models.MotorGyartok>>(motogy);
        }

        private MotorGyartok GetMGyModel(int id)
        {
            motorGyartok oneMgy = logic.MotorGyartokGetOne(id);
            return mapper.Map<Data.motorGyartok, Models.MotorGyartok>(oneMgy);
        }

        // GET: MotorGyartok
        public ActionResult Index()
        {
            ViewData["editAction"] = "AddNew";
            return View("MotorGyartoIndex",vm);
        }

        // GET: MotorGyartok/Details/5
        public ActionResult Details(int id)
        {
            return View("MotorGyartoDetails",GetMGyModel(id));
        }
        //GET: MotorGyartok/Remove/5
        public ActionResult Remove(int id)
        {
            TempData["editResult"] = "Delete Failed";
            if (logic.MotorGyDelete(id)) TempData["editResult"] = "Delete OK";

            return RedirectToAction(nameof(Index));
        }
        //GET: MotorGyartok/Edit/5
        public ActionResult Edit(int id)
        {
            ViewData["editAction"] = "Edit";
            vm.EditedMGy = GetMGyModel(id);
            return View("MotorGyartoIndex", vm);
        }
        [HttpPost]
        public ActionResult Edit(MotorGyartok mgy,string editAction)
        {
            if (ModelState.IsValid && mgy!=null)
            {
                TempData["editResult"] = "Edit OK";
                if (editAction=="AddNew")
                {
                    logic.MotorGyartokCreate(mgy.Nev, mgy.AlapitasEve, mgy.Nemzetiseg, mgy.Kozpont, mgy.LoEro, mgy.GyozelmekSzama);
                }
                else
                {
                    bool siker = logic.MotorGyUpdate(mgy.Id, mgy.Nev, mgy.AlapitasEve, mgy.Nemzetiseg, mgy.Kozpont, mgy.LoEro, mgy.GyozelmekSzama);
                    if (!siker) TempData["editResult"] = "Edit Fail";

                }
                return RedirectToAction(nameof(Index));
            }
            else
            {
                ViewData["editAction"] = "Edit";
                vm.EditedMGy = mgy;
                return View("MotorGyartoIndex", vm);
            }
        }
        
    }
}
