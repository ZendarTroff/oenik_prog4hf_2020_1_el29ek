﻿// <copyright file="LogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyF1.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Moq;
    using MyF1.Data;
    using MyF1.Repository;
    using NUnit.Framework;

    /// <summary>
    /// Logic test.
    /// </summary>
    [TestFixture]
    public class LogicTest
    {
        private Mock<IRepository> motorGyartoRepoMoq;
        private Mock<IRepository> csapatRepoMoq;
        private Mock<IRepository> pilotaRepoMoq;
        private Mock<IRepository> csapatXpilotaRepoMoq;

        /// <summary>
        /// Test setup.
        /// </summary>
        [SetUp]
        public void Setup()
        {
            this.motorGyartoRepoMoq = new Mock<IRepository>();
            this.csapatRepoMoq = new Mock<IRepository>();
            this.pilotaRepoMoq = new Mock<IRepository>();
            this.csapatXpilotaRepoMoq = new Mock<IRepository>();
            List<motorGyartok> motoList = new List<motorGyartok>()
            {
           new motorGyartok() { ID = 1, gyartoNev = "Testname1", alapitasEve = 1, nemzetiseg = "TestNationality1", kozpont = "TestKozpont1", loEro = 1, gyozelmekSzama = 1 },
           new motorGyartok() { ID = 2, gyartoNev = "Testname2", alapitasEve = 2, nemzetiseg = "TestNationality2", kozpont = "TestKozpont2", loEro = 2, gyozelmekSzama = 2 },
           new motorGyartok() { ID = 3, gyartoNev = "Testname3", alapitasEve = 3, nemzetiseg = "TestNationality3", kozpont = "TestKozpont3", loEro = 3, gyozelmekSzama = 3 },
            };
            this.motorGyartoRepoMoq.Setup(x => x.MotorgyartokGetAll()).Returns(motoList.AsQueryable());

            // motorGyartok mgy1 = new motorGyartok() { ID = 1, gyartoNev = "Testname1", alapitasEve = 1, nemzetiseg = "TestNationality1", kozpont = "TestKozpont1", loEro = 1, gyozelmekSzama = 1 };
            // motorGyartok mgy2 = new motorGyartok() { ID = 2, gyartoNev = "Testname2", alapitasEve = 2, nemzetiseg = "TestNationality2", kozpont = "TestKozpont2", loEro = 2, gyozelmekSzama = 2 };
            // motorGyartok mgy3 = new motorGyartok() { ID = 3, gyartoNev = "Testname3", alapitasEve = 3, nemzetiseg = "TestNationality3", kozpont = "TestKozpont3", loEro = 3, gyozelmekSzama = 3 };
            // this.motorGyartoRepoMoq.Setup(x => x.MotorgyartokGetAll()).Returns(new[] { mgy1, mgy2, mgy3 });
            List<csapatok> csapatList = new List<csapatok>()
            {
            new csapatok() { ID = 1, csapatNev = "Testname1", alapitasEve = 1, kozpont = "TestKozpont1", csapatSzin = "TestSzin1", gyozelmekSzama = 1, bajnokiCimekSzama = 1, gyartoID = 1 },
            new csapatok() { ID = 2, csapatNev = "Testname2", alapitasEve = 2, kozpont = "TestKozpont2", csapatSzin = "TestSzin2", gyozelmekSzama = 2, bajnokiCimekSzama = 2, gyartoID = 2 },
            new csapatok() { ID = 3, csapatNev = "Testname3", alapitasEve = 3, kozpont = "TestKozpont3", csapatSzin = "TestSzin3", gyozelmekSzama = 1, bajnokiCimekSzama = 1, gyartoID = 3 },
            new csapatok() { ID = 4, csapatNev = "Testname4", alapitasEve = 1, kozpont = "TestKozpont4", csapatSzin = "TestSzin4", gyozelmekSzama = 3, bajnokiCimekSzama = 3, gyartoID = 2 },
            new csapatok() { ID = 5, csapatNev = "Testname5", alapitasEve = 2, kozpont = "TestKozpont5", csapatSzin = "TestSzin2", gyozelmekSzama = 2, bajnokiCimekSzama = 1, gyartoID = 1 },
            new csapatok() { ID = 6, csapatNev = "Testname6", alapitasEve = 3, kozpont = "TestKozpont6", csapatSzin = "TestSzin3", gyozelmekSzama = 1, bajnokiCimekSzama = 2, gyartoID = 3 },
            };
            this.csapatRepoMoq.Setup(x => x.CsapatokGetAll()).Returns(csapatList.AsQueryable());

            // csapatok cs1 = new csapatok() { ID = 1, csapatNev = "Testname1", alapitasEve = 1, kozpont = "TestKozpont1", csapatSzin = "TestSzin1", gyozelmekSzama = 1, bajnokiCimekSzama = 1, gyartoID = 1 };
            // csapatok cs2 = new csapatok() { ID = 2, csapatNev = "Testname2", alapitasEve = 2, kozpont = "TestKozpont2", csapatSzin = "TestSzin2", gyozelmekSzama = 2, bajnokiCimekSzama = 2, gyartoID = 2 };
            // csapatok cs3 = new csapatok() { ID = 3, csapatNev = "Testname3", alapitasEve = 3, kozpont = "TestKozpont3", csapatSzin = "TestSzin3", gyozelmekSzama = 1, bajnokiCimekSzama = 1, gyartoID = 3 };
            // csapatok cs4 = new csapatok() { ID = 4, csapatNev = "Testname4", alapitasEve = 1, kozpont = "TestKozpont4", csapatSzin = "TestSzin4", gyozelmekSzama = 3, bajnokiCimekSzama = 3, gyartoID = 2 };
            // csapatok cs5 = new csapatok() { ID = 5, csapatNev = "Testname5", alapitasEve = 2, kozpont = "TestKozpont5", csapatSzin = "TestSzin2", gyozelmekSzama = 2, bajnokiCimekSzama = 1, gyartoID = 1 };
            // csapatok cs6 = new csapatok() { ID = 6, csapatNev = "Testname6", alapitasEve = 3, kozpont = "TestKozpont6", csapatSzin = "TestSzin3", gyozelmekSzama = 1, bajnokiCimekSzama = 2, gyartoID = 3 };
            // this.csapatRepoMoq.Setup(x => x.CsapatokGetAll()).Returns(new[] { cs1, cs2, cs3, cs4, cs5, cs6 });
            List<pilota> pilotaList = new List<pilota>()
            {
            new pilota() { pilotaID = 1, nev = "Testname1", karrierKezdesEve = 1, nemzetiseg = "TestNationality1", gyozelmekSzama = 1, bajnokiCimekSzama = 1 },
            new pilota() { pilotaID = 2, nev = "Testname2", karrierKezdesEve = 2, nemzetiseg = "TestNationality2", gyozelmekSzama = 3, bajnokiCimekSzama = 2 },
            new pilota() { pilotaID = 3, nev = "Testname3", karrierKezdesEve = 1, nemzetiseg = "TestNationality3", gyozelmekSzama = 2, bajnokiCimekSzama = 3 },
            new pilota() { pilotaID = 4, nev = "Testname4", karrierKezdesEve = 3, nemzetiseg = "TestNationality4", gyozelmekSzama = 1, bajnokiCimekSzama = 1 },
            };
            this.pilotaRepoMoq.Setup(x => x.PilotaGetAll()).Returns(pilotaList.AsQueryable());

            // pilota p1 = new pilota() { pilotaID = 1, nev = "Testname1", karrierKezdesEve = 1, nemzetiseg = "TestNationality1", gyozelmekSzama = 1, bajnokiCimekSzama = 1 };
            // pilota p2 = new pilota() { pilotaID = 2, nev = "Testname2", karrierKezdesEve = 2, nemzetiseg = "TestNationality2", gyozelmekSzama = 3, bajnokiCimekSzama = 2 };
            // pilota p3 = new pilota() { pilotaID = 3, nev = "Testname3", karrierKezdesEve = 1, nemzetiseg = "TestNationality3", gyozelmekSzama = 2, bajnokiCimekSzama = 3 };
            // pilota p4 = new pilota() { pilotaID = 4, nev = "Testname4", karrierKezdesEve = 3, nemzetiseg = "TestNationality4", gyozelmekSzama = 1, bajnokiCimekSzama = 1 };
            // this.pilotaRepoMoq.Setup(x => x.PilotaGetAll()).Returns(new[] { p1, p2, p3, p4 });
            List<csapatXpilota> cxpList = new List<csapatXpilota>()
            {
            new csapatXpilota() { szerzodesID = 1, csapatID = 1, pilotaID = 1 },
            new csapatXpilota() { szerzodesID = 2, csapatID = 2, pilotaID = 2 },
            new csapatXpilota() { szerzodesID = 3, csapatID = 3, pilotaID = 4 },
            new csapatXpilota() { szerzodesID = 4, csapatID = 4, pilotaID = 3 },
            new csapatXpilota() { szerzodesID = 5, csapatID = 5, pilotaID = 1 },
            };
            this.csapatXpilotaRepoMoq.Setup(x => x.CsapatXpilotaGetAll()).Returns(cxpList.AsQueryable());

            // csapatXpilota cxp1 = new csapatXpilota() { szerzodesID = 1, csapatID = 1, pilotaID = 1 };
            // csapatXpilota cxp2 = new csapatXpilota() { szerzodesID = 2, csapatID = 2, pilotaID = 2 };
            // csapatXpilota cxp3 = new csapatXpilota() { szerzodesID = 3, csapatID = 3, pilotaID = 4 };
            // csapatXpilota cxp4 = new csapatXpilota() { szerzodesID = 4, csapatID = 4, pilotaID = 3 };
            // csapatXpilota cxp5 = new csapatXpilota() { szerzodesID = 5, csapatID = 5, pilotaID = 1 };
            // this.csapatXpilotaRepoMoq.Setup(x => x.CsapatXpilotaGetAll()).Returns(new[] { cxp1, cxp2, cxp3, cxp4, cxp5 });
        }

        /// <summary>
        /// Correct number of MotorGyartok test method.
        /// </summary>
        [Test]
        public void MotorGyartokGetAll_ValidDataReturn()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            var result = logic.MotorgyartokGetAll();
            Assert.That(result.Count(), Is.EqualTo(3));
        }

        /// <summary>
        /// Not null MotorGyartok test method.
        /// </summary>
        [Test]
        public void MotorGyartokGetAll_NotNullValidDataReturn()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            var result = logic.MotorgyartokGetAll();
            Assert.That(result.Count(), Is.Not.Null);
        }

        /// <summary>
        /// Correct number of Csapatok test method.
        /// </summary>
        [Test]
        public void CsapatokGetAll_ValidDataReturn()
        {
            Logic logic = new Logic(this.csapatRepoMoq.Object);
            var result = logic.CsapatokGetAll();
            Assert.That(result.Count(), Is.EqualTo(6));
        }

        /// <summary>
        /// Not null Csapatok test method.
        /// </summary>
        [Test]
        public void CsapatokGetAll_NotNullValidDataReturn()
        {
            Logic logic = new Logic(this.csapatRepoMoq.Object);
            var result = logic.CsapatokGetAll();
            Assert.That(result.Count(), Is.Not.Null);
        }

        /// <summary>
        /// Correct number of Pilota test method.
        /// </summary>
        [Test]
        public void PilotaGetAll_ValidDataReturn()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            var result = logic.PilotaGetAll();
            Assert.That(result.Count(), Is.EqualTo(4));
        }

        /// <summary>
        /// Not null Pilota test method.
        /// </summary>
        [Test]
        public void PilotaGetAll_NotNullValidDataReturn()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            var result = logic.PilotaGetAll();
            Assert.That(result.Count(), Is.Not.Null);
        }

        /// <summary>
        /// Correct number of CsapatXPilota test method.
        /// </summary>
        [Test]
        public void CsapatXPilotaGetAll_ValidDataReturn()
        {
            Logic logic = new Logic(this.csapatXpilotaRepoMoq.Object);
            var result = logic.CsapatXpilotaGetAll();
            Assert.That(result.Count(), Is.EqualTo(5));
        }

        /// <summary>
        /// Not null CsapatXPilota test method.
        /// </summary>
        [Test]
        public void CsapatXPilotaGetAll_NotNullValidDataReturn()
        {
            Logic logic = new Logic(this.csapatXpilotaRepoMoq.Object);
            var result = logic.CsapatXpilotaGetAll();
            Assert.That(result.Count(), Is.Not.Null);
        }

        /// <summary>
        /// Single motorgyarto test back method.
        /// </summary>
        [Test]
        public void MotorgyartokGetOneTest()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            logic.MotorGyartokGetOne(2);
            this.motorGyartoRepoMoq.Verify(x => x.MotorGyartokGetOne(2), Times.Once);
        }

        /// <summary>
        /// Single csapatok test back method.
        /// </summary>
        [Test]
        public void CsapatokGetOneTest()
        {
            Logic logic = new Logic(this.csapatRepoMoq.Object);
            logic.CsapatokGetOne(4);
            this.csapatRepoMoq.Verify(x => x.CsapatokGetOne(4), Times.Once);
        }

        /// <summary>
        /// Single pilota test back method.
        /// </summary>
        [Test]
        public void PilotaGetOneTest()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            logic.PilotaGetOne(1);
            this.pilotaRepoMoq.Verify(x => x.PilotaGetOne(1), Times.Once);
        }

        /// <summary>
        /// Single csapatxpilota tet back method.
        /// </summary>
        [Test]
        public void CsapatXPilotakGetOneTest()
        {
            Logic logic = new Logic(this.csapatXpilotaRepoMoq.Object);
            logic.CsapatXpilotaGetOne(3);
            this.csapatXpilotaRepoMoq.Verify(x => x.CsapatXpilotaGetOne(3), Times.Once);
        }

        /// <summary>
        /// New motorGyartok create test method.
        /// </summary>
        [Test]
        public void MotorGyartokCreateTest()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            logic.MotorGyartokCreate("testname", 1932, "testnationality", "testkozpont", 440, 30);
            this.motorGyartoRepoMoq.Verify(x => x.MotorGyartokCreate("testname", 1932, "testnationality", "testkozpont", 440, 30), Times.Once);
        }

        /// <summary>
        /// New csapatok create test method.
        /// </summary>
        [Test]
        public void CsapatokCreateTest()
        {
            Logic logic = new Logic(this.csapatRepoMoq.Object);
            logic.CsapatokCreate("testname", 1955, "testkozpont", "testSzin", 23, 2, 1);

            this.csapatRepoMoq.Verify(x => x.CsapatokCreate("testname", 1955, "testkozpont", "testSzin", 23, 2, 1), Times.Once);
        }

        /// <summary>
        /// New pilota create test method.
        /// </summary>
        [Test]
        public void PilotaCreateTest()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            logic.PilotaCreate("testname", 1992, "testnationality", 6, 0);

            this.pilotaRepoMoq.Verify(x => x.PilotaCreate("testname", 1992, "testnationality", 6, 0), Times.Once);
        }

        /// <summary>
        /// New csapatXpilota create test method.
        /// </summary>
        [Test]
        public void CsapatXPilotaCreateTest()
        {
            Logic logic = new Logic(this.csapatXpilotaRepoMoq.Object);
            logic.CsapatXPilotaCreate(1, 2);
            this.csapatXpilotaRepoMoq.Verify(x => x.CsapatXPilotaCreate(1, 2), Times.Once);
        }

        /// <summary>
        /// MotorGyartok update test method.
        /// </summary>
        [Test]
        public void MotorGyartokUpdateTest()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            logic.MotorGyartokUpdate(2, "alapitasEve", "1935");
            this.motorGyartoRepoMoq.Verify(x => x.MotorGyartokUpdate(2, "alapitasEve", "1935"), Times.Once);
        }

        /// <summary>
        /// Csapatok update test method.
        /// </summary>
        [Test]
        public void CsapatokUpdateTest()
        {
            Logic logic = new Logic(this.csapatRepoMoq.Object);
            logic.CsapatokUpdate(3, "kozpont", "updatedKozpont");
            this.csapatRepoMoq.Verify(x => x.CsapatokUpdate(3, "kozpont", "updatedKozpont"), Times.Once);
        }

        /// <summary>
        /// Pilota update test method.
        /// </summary>
        [Test]
        public void PilotakUpdateTest()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            logic.PilotaUpdate(2, "gyozelmekSzama", "7");
            this.pilotaRepoMoq.Verify(x => x.PilotaUpdate(2, "gyozelmekSzama", "7"), Times.Once);
        }

        /// <summary>
        /// CsapatXPilota update test method.
        /// </summary>
        [Test]
        public void CsapatXPilotakUpdateTest()
        {
            Logic logic = new Logic(this.csapatXpilotaRepoMoq.Object);
            logic.CsapatXPilotaUpdate(5, "csapatID", "6");
            this.csapatXpilotaRepoMoq.Verify(x => x.CsapatXPilotaUpdate(5, "csapatID", "6"), Times.Once);
        }

        /// <summary>
        /// MotorGyartok delete test method.
        /// </summary>
        [Test]
        public void MotorGyartokDeleteTest()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            logic.MotorGyartokDelete(2);
            this.motorGyartoRepoMoq.Verify(x => x.MotorGyartokDelete(2), Times.Once);
        }

        /// <summary>
        /// Csapatok delete test metod.
        /// </summary>
        [Test]
        public void CsapatokDeleteTest()
        {
            Logic logic = new Logic(this.csapatRepoMoq.Object);
            logic.CsapatokDelete(1);
            this.csapatRepoMoq.Verify(x => x.CsapatokDelete(1), Times.Once);
        }

        /// <summary>
        /// Pilota delete test method.
        /// </summary>
        [Test]
        public void PilotaDeleteTest()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            logic.PilotaDelete(4);
            this.pilotaRepoMoq.Verify(x => x.PilotaDelete(4), Times.Once);
        }

        /// <summary>
        /// CsapatXpilota delete test method.
        /// </summary>
        [Test]
        public void CsapatXPilotaDeleteTest()
        {
            Logic logic = new Logic(this.csapatXpilotaRepoMoq.Object);
            logic.CsapatXPilotaDelete(3);
            this.csapatXpilotaRepoMoq.Verify(x => x.CsapatXPilotaDelete(3), Times.Once);
        }

        /// <summary>
        /// MostSuccessfulPilot Test method.
        /// </summary>
        [Test]
        public void MostSuccessfulPilotTest()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            string res = logic.MostSuccessfulPilot();

            Assert.That(res, Is.EqualTo("Testname3"));
        }

        /// <summary>
        /// Pilots at least 4 wc and their teams where have contract test.
        /// </summary>
        [Test]
        public void PilotsandTeamAtLeast4Pilotwc()
        {
            Logic logic = new Logic(this.pilotaRepoMoq.Object);
            var res = logic.PilotsandTeamAtLeast4Pilotwc(4);
            Assert.That(res, Is.Not.Null);
        }

        /// <summary>
        /// NumberOfMostTeamWinsInGrandPrixes Test method.
        /// </summary>
        [Test]
        public void NumberOfMostTeamWinsInGrandPrixesTest()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            int res = logic.NumberOfMostTeamWinsInGrandPrixes();

            Assert.That(res, Is.EqualTo(3));
        }

        /// <summary>
        /// Most success constructor test.
        /// </summary>
        public void MostSuccessConstructorByMostSuccessPilot()
        {
            Logic logic = new Logic(this.motorGyartoRepoMoq.Object);
            string res = logic.ConstructorOfTheMostVictoriousPilot();
            Assert.That(res, Is.Not.Empty);
        }
    }
}
