﻿// <copyright file="Repository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyF1.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyF1.Data;

    /// <summary>
    /// Repository.
    /// </summary>
    public class Repository : IRepository, IDisposable
    {
        private AdatbazisEntities entities;

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        public Repository()
        {
            this.entities = new AdatbazisEntities();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Repository"/> class.
        /// </summary>
        /// <param name="entities">Entity model.</param>
        public Repository(AdatbazisEntities entities)
        {
            this.entities = entities;
        }

        /// <summary>
        /// Dispose method.
        /// </summary>
        public void Dispose()
        {
            this.entities.Dispose();
        }

        /// <summary>
        /// csapatok create method.
        /// </summary>
        /// <param name="csapatNev">new csapatnev.</param>
        /// <param name="alapitasEve">new alapitas eve.</param>
        /// <param name="kozpont">new kozpont.</param>
        /// <param name="csapatSzin">new csapatszin.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        /// <param name="bajnokiCimekSzama">new bajnoki cimek szama.</param>
        /// <param name="gyartoID">new gyarto id.</param>
        public void CsapatokCreate(string csapatNev, int alapitasEve, string kozpont, string csapatSzin, int gyozelmekSzama, int bajnokiCimekSzama, int gyartoID)
        {
            int lastID = this.entities.csapatok.Count();
            csapatok csapat = new csapatok()
            {
                ID = lastID + 1,
                csapatNev = csapatNev,
                alapitasEve = alapitasEve,
                kozpont = kozpont,
                csapatSzin = csapatSzin,
                gyozelmekSzama = gyozelmekSzama,
                bajnokiCimekSzama = bajnokiCimekSzama,
                gyartoID = gyartoID,
            };
            this.entities.csapatok.Add(csapat);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">cspatok delete.</param>
        public void CsapatokDelete(int id)
        {
            csapatok csapat = this.entities.csapatok.Single(x => x.ID == id);
            this.entities.csapatok.Remove(csapat);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// csapatok get method.
        /// </summary>
        /// <returns>csapatok.</returns>
        public IQueryable<csapatok> CsapatokGetAll()
        {
            return this.entities.csapatok.AsQueryable();

            // return this.entities.csapatok;
        }

        /// <summary>
        /// csapatok get one method.
        /// </summary>
        /// <param name="id">csapat id.</param>
        /// <returns>csapat.</returns>
        public csapatok CsapatokGetOne(int id)
        {
            return this.CsapatokGetAll().Single(x => x.ID == id);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        public void CsapatokUpdate(int id, string field, string newData)
        {
            csapatok cs = this.entities.csapatok.Single(x => x.ID == id);

            switch (field)
            {
                case "ID":
                    cs.ID = int.Parse(newData);
                    break;
                case "csapatNev":
                    cs.csapatNev = newData;
                    break;
                case "alapitasEve":
                    cs.alapitasEve = int.Parse(newData);
                    break;
                case "kozpont":
                    cs.kozpont = newData;
                    break;
                case "csapatSzin":
                    cs.csapatSzin = newData;
                    break;
                case "gyozelmekSzama":
                    cs.gyozelmekSzama = int.Parse(newData);
                    break;
                case "bajnokiCimekSzama":
                    cs.bajnokiCimekSzama = int.Parse(newData);
                    break;
                case "gyartoID":
                    cs.gyartoID = int.Parse(newData);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }

        /// <summary>
        /// csapatxpilota create method.
        /// </summary>
        /// <param name="csapatID">new csapat id.</param>
        /// <param name="pilotaID">new pilota id.</param>
        public void CsapatXPilotaCreate(int csapatID, int pilotaID)
        {
            int lastID = this.entities.csapatXpilota.Count();
            csapatXpilota cxp = new csapatXpilota()
            {
                szerzodesID = lastID + 1,
                csapatID = csapatID,
                pilotaID = pilotaID,
            };
            this.entities.csapatXpilota.Add(cxp);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">csapatXpilota delete.</param>
        public void CsapatXPilotaDelete(int id)
        {
            csapatXpilota cXp = this.entities.csapatXpilota.Single(x => x.szerzodesID == id);
            this.entities.csapatXpilota.Remove(cXp);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// get method.
        /// </summary>
        /// <returns>csapatXpilota.</returns>
        public IQueryable<csapatXpilota> CsapatXpilotaGetAll()
        {
            return this.entities.csapatXpilota.AsQueryable();

            // return this.entities.csapatXpilota;
        }

        /// <summary>
        /// csapatXpilota get one method.
        /// </summary>
        /// <param name="id">csapatxpilota id.</param>
        /// <returns>csapatxpilota.</returns>
        public csapatXpilota CsapatXpilotaGetOne(int id)
        {
            return this.CsapatXpilotaGetAll().Single(x => x.szerzodesID == id);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        public void CsapatXPilotaUpdate(int id, string field, string newData)
        {
            csapatXpilota cxp = this.entities.csapatXpilota.Single(x => x.szerzodesID == id);
            switch (field)
            {
                case "szerzodesID":
                    cxp.szerzodesID = int.Parse(newData);
                    break;
                case "csapatID":
                    cxp.csapatID = int.Parse(newData);
                    break;
                case "pilotaID":
                    cxp.pilotaID = int.Parse(newData);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }

        /// <summary>
        /// Motorgyartok create method.
        /// </summary>
        /// <param name="gyartoNev">new gyartonev.</param>
        /// <param name="alapitasEve">new alapitas eve.</param>
        /// <param name="nemzetiseg">new nemzetiseg.</param>
        /// <param name="kozpont">new kozpont.</param>
        /// <param name="loEro">new loero.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        public void MotorGyartokCreate(string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama)
        {
            //int lastID = (int)this.entities.motorGyartok.Last().ID;
            //int maxid = (int)this.repository.FactoriesGetAll().Select(x => x.ID).Max()

            int m = (int)MotorgyartokGetAll().Select(x => x.ID).Max();
            motorGyartok mgy = new motorGyartok()
            {
                ID = m + 1,
                gyartoNev = gyartoNev,
                alapitasEve = alapitasEve,
                nemzetiseg = nemzetiseg,
                kozpont = kozpont,
                loEro = loEro,
                gyozelmekSzama = gyozelmekSzama,
            };
            this.entities.motorGyartok.Add(mgy);
            this.entities.SaveChanges();
        }
        ///// <summary>
        ///// create method.
        ///// </summary>
        ///// <param name="newMotorGyartok">motorGyartok create.</param>
        // public void MotorGyartokCreate(motorGyartok newMotorGyartok)
        // {
        //    this.entities.motorGyartok.Add(newMotorGyartok);
        //    this.entities.SaveChanges();
        // }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">motorGyartok delete.</param>
        public void MotorGyartokDelete(int id)
        {
            AdatbazisEntities motoentiti = new AdatbazisEntities();
            motorGyartok mgy = motoentiti.motorGyartok.Single(x => x.ID == id);
            motoentiti.motorGyartok.Remove(mgy);
            motoentiti.SaveChanges();
            // this.entities.motorGyartok.Remove(mgy);
            //this.entities.SaveChanges();
        }

        //public bool MotorGyDelete(int id)
        //{
        //    //motorGyartok mgy = MotorGyartokGetOne(id);
        //    motorGyartok mgy = this.entities.motorGyartok.Single(x => x.ID == id);
        //    if (mgy == null) return false;
        //    this.entities.motorGyartok.Remove(mgy);
        //    this.entities.SaveChanges();
        //    return true;


        //}
        public bool MotorGyDelete(int id)
        {
            motorGyartok mgy = this.entities.motorGyartok.Single(x => x.ID == id);

           List<csapatok> csapat = CsapatokGetAll().ToList();
           List<csapatXpilota> cxp = CsapatXpilotaGetAll().ToList();

            if (mgy.ID==id)
            {
                foreach (csapatok item in csapat)
                {
                    if (item.gyartoID == id)
                    {
                        foreach (csapatXpilota item2 in cxp)
                        {
                            if (item2.csapatID == item.ID)
                            {
                                //foreach (var item3 in pi)
                                //{
                                //    if (item3.pilotaID==item2.pilotaID)
                                //    {
                                //        PilotaDelete((int)item3.pilotaID);
                                //    }
                                //}
                                CsapatXPilotaDelete((int)item2.szerzodesID);
                            }
                        }

                        CsapatokDelete((int)item.ID);
                    }
                }
                this.entities.motorGyartok.Remove(mgy);
                this.entities.SaveChanges();

                return true;
            }
            return false;
        }

        /// <summary>
        /// get method.
        /// </summary>
        /// <returns>motorGyartok.</returns>
        public IQueryable<motorGyartok> MotorgyartokGetAll()
        {
            return this.entities.motorGyartok.AsQueryable();

            // return this.entities.motorGyartok;
        }

        /// <summary>
        /// motorgyartok get one method.
        /// </summary>
        /// <param name="id">motorgyarto id.</param>
        /// <returns>motorgyarto.</returns>
        public motorGyartok MotorGyartokGetOne(int id)
        {
            return this.MotorgyartokGetAll().Single(x => x.ID == id);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        public void MotorGyartokUpdate(int id, string field, string newData)
        {
            motorGyartok mgy = this.entities.motorGyartok.Single(x => x.ID == id);
            switch (field)
            {
                case "ID":
                    mgy.ID = int.Parse(newData);
                    break;
                case "gyartoNev":
                    mgy.gyartoNev = newData;
                    break;
                case "alapitasEve":
                    mgy.alapitasEve = int.Parse(newData);
                    break;
                case "nemzetiseg":
                    mgy.nemzetiseg = newData;
                    break;
                case "kozpont":
                    mgy.kozpont = newData;
                    break;
                case "loEro":
                    mgy.loEro = int.Parse(newData);
                    break;
                case "gyozelmekSzama":
                    mgy.gyozelmekSzama = int.Parse(newData);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }

        public bool MotorGyUpdate(int id, string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama)
        {
            motorGyartok mgy = this.entities.motorGyartok.Single(x => x.ID == id);
            if (mgy == null) return false;

                mgy.gyartoNev = gyartoNev;
                mgy.alapitasEve = alapitasEve;
                mgy.nemzetiseg = nemzetiseg;
                mgy.kozpont = kozpont;
                mgy.loEro = loEro;
                mgy.gyozelmekSzama = gyozelmekSzama;

                this.entities.SaveChanges();
                return true;

        }

        /// <summary>
        /// pilota create method.
        /// </summary>
        /// <param name="nev">new nev.</param>
        /// <param name="karrierKezdesEve">new karrier kezdete eve.</param>
        /// <param name="nemzetiseg">new nemzetiseg.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        /// <param name="bajnokiCimekSzama">new bajnoki cimek szama.</param>
        public void PilotaCreate(string nev, int karrierKezdesEve, string nemzetiseg, int gyozelmekSzama, int bajnokiCimekSzama)
        {
            int lastID = this.entities.pilota.Count();
            pilota pilota = new pilota()
            {
                pilotaID = lastID + 1,
                nev = nev,
                karrierKezdesEve = karrierKezdesEve,
                nemzetiseg = nemzetiseg,
                gyozelmekSzama = gyozelmekSzama,
                bajnokiCimekSzama = bajnokiCimekSzama,
            };
            this.entities.pilota.Add(pilota);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// delete method.
        /// </summary>
        /// <param name="id">pilota delete.</param>
        public void PilotaDelete(int id)
        {
            pilota p = this.entities.pilota.Single(x => x.pilotaID == id);
            this.entities.pilota.Remove(p);
            this.entities.SaveChanges();
        }

        /// <summary>
        /// get method.
        /// </summary>
        /// <returns>pilota.</returns>
        public IQueryable<pilota> PilotaGetAll()
        {
            return this.entities.pilota.AsQueryable();

            // return this.entities.pilota;
        }

        /// <summary>
        /// pilta get one method.
        /// </summary>
        /// <param name="id">pilota id.</param>
        /// <returns>pilota.</returns>
        public pilota PilotaGetOne(int id)
        {
            return this.PilotaGetAll().Single(x => x.pilotaID == id);
        }

        /// <summary>
        /// update method.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        public void PilotaUpdate(int id, string field, string newData)
        {
            pilota p = this.entities.pilota.Single(x => x.pilotaID == id);
            switch (field)
            {
                case "pilotaID":
                    p.pilotaID = int.Parse(newData);
                    break;
                case "nev":
                    p.nev = newData;
                    break;
                case "karrierKezdesEve":
                    p.karrierKezdesEve = int.Parse(newData);
                    break;
                case "nemzetiseg":
                    p.nemzetiseg = newData;
                    break;
                case "gyozelmekSzama":
                    p.gyozelmekSzama = int.Parse(newData);
                    break;
                case "bajnokiCimekSzama":
                    p.bajnokiCimekSzama = int.Parse(newData);
                    break;
                default:
                    break;
            }

            this.entities.SaveChanges();
        }
    }
}
