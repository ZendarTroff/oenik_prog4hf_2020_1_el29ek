﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace MyF1.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using MyF1.Data;

    /// <summary>
    /// IRepository intreface.
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// motorgyartok get metod.
        /// </summary>
        /// <returns>motorGyartok.</returns>
        IQueryable<motorGyartok> MotorgyartokGetAll();

        /// <summary>
        /// motorgyartok get one method.
        /// </summary>
        /// <param name="id">motorgyarto id.</param>
        /// <returns>motorgyarto.</returns>
        motorGyartok MotorGyartokGetOne(int id);

        ///// <summary>
        ///// Motorgyartok create metod.
        ///// </summary>
        ///// <param name="newMotorGyartok">New MotorGyartok class.</param>
        // void MotorGyartokCreate(motorGyartok newMotorGyartok);

        /// <summary>
        /// motorgyartok create method.
        /// </summary>
        /// <param name="gyartoNev">new gyartonev.</param>
        /// <param name="alapitasEve">new alapitaseve.</param>
        /// <param name="nemzetiseg">new nemzetiseg.</param>
        /// <param name="kozpont">new kozpont.</param>
        /// <param name="loEro">new loero.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        void MotorGyartokCreate(string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama);

        /// <summary>
        /// Motorgyartok update metod.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void MotorGyartokUpdate(int id, string field, string newData);

        bool MotorGyUpdate(int id, string gyartoNev, int alapitasEve, string nemzetiseg, string kozpont, int loEro, int gyozelmekSzama);
        /// <summary>
        /// Motorgyartok delete metod.
        /// </summary>
        /// <param name="id">Delete Motorgyartok.</param>
        void MotorGyartokDelete(int id);

        bool MotorGyDelete(int id);

        /// <summary>
        /// csapatok get metod.
        /// </summary>
        /// <returns>csapatok.</returns>
        IQueryable<csapatok> CsapatokGetAll();

        /// <summary>
        /// csapatok get one method.
        /// </summary>
        /// <param name="id">csapat id.</param>
        /// <returns>csapat.</returns>
        csapatok CsapatokGetOne(int id);

        /// <summary>
        /// csapat create method.
        /// </summary>
        /// <param name="csapatNev">new csapatnev.</param>
        /// <param name="alapitasEve">new alapitas eve.</param>
        /// <param name="kozpont">new kozpont.</param>
        /// <param name="csapatSzin">new csapatszin.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        /// <param name="bajnokiCimekSzama">new bajnoki cimek szama.</param>
        /// <param name="gyartoID">new gyarto id.</param>
        void CsapatokCreate(string csapatNev, int alapitasEve, string kozpont, string csapatSzin, int gyozelmekSzama, int bajnokiCimekSzama, int gyartoID);

        /// <summary>
        /// csapatok update mehod.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void CsapatokUpdate(int id, string field, string newData);

        /// <summary>
        /// csapatok delete method.
        /// </summary>
        /// <param name="id">delete id.</param>
        void CsapatokDelete(int id);

        /// <summary>
        /// pilota get method.
        /// </summary>
        /// <returns>pilota.</returns>
        IQueryable<pilota> PilotaGetAll();

        /// <summary>
        /// pilota get one method.
        /// </summary>
        /// <param name="id">pilota id.</param>
        /// <returns>pilota.</returns>
        pilota PilotaGetOne(int id);

        /// <summary>
        /// pilota create method.
        /// </summary>
        /// <param name="nev">new nev.</param>
        /// <param name="karrierKezdesEve">new karrier kezdete.</param>
        /// <param name="nemzetiseg">new nemzetiseg.</param>
        /// <param name="gyozelmekSzama">new gyozelmek szama.</param>
        /// <param name="bajnokiCimekSzama">new bajnoki cimek szama.</param>
        void PilotaCreate(string nev, int karrierKezdesEve, string nemzetiseg, int gyozelmekSzama, int bajnokiCimekSzama);

        /// <summary>
        /// Pilota update method.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void PilotaUpdate(int id, string field, string newData);

        /// <summary>
        /// Pilota delete method.
        /// </summary>
        /// <param name="id">delete id.</param>
        void PilotaDelete(int id);

        /// <summary>
        /// csapataXpilota get method.
        /// </summary>
        /// <returns>csapatXpilota.</returns>
        IQueryable<csapatXpilota> CsapatXpilotaGetAll();

        /// <summary>
        /// csapatxpilota get one method.
        /// </summary>
        /// <param name="id">csapatXpilota id.</param>
        /// <returns>csapatXpilota.</returns>
        csapatXpilota CsapatXpilotaGetOne(int id);

        /// <summary>
        /// csapatxpilota create method.
        /// </summary>
        /// <param name="csapatID">new csapat id.</param>
        /// <param name="pilotaID">new pilota id.</param>
        void CsapatXPilotaCreate(int csapatID, int pilotaID);

        /// <summary>
        /// csapatXpilota update method.
        /// </summary>
        /// <param name="id">row id.</param>
        /// <param name="field">field name.</param>
        /// <param name="newData">new data.</param>
        void CsapatXPilotaUpdate(int id, string field, string newData);

        /// <summary>
        /// csapatXpilota delete method.
        /// </summary>
        /// <param name="id">delete id.</param>
        void CsapatXPilotaDelete(int id);
    }
}
